// tailwind.config.js
module.exports = {
  purge: [
    // Use *.tsx if using TypeScript
    "./pages/**/*.js",
    "./components/**/*.js",
  ],
  plugins: [require("@tailwindcss/forms")],
  theme: {
    extend: {
      backgroundImage: theme => ({
       'age-backimage': "url('/mbg.jpg')",
       'main-pattern': "url('/bg_pattern.svg')",
       'main-pattern-t': "url('/bg_pattern_t.svg')",
       'main-pattern-b': "url('/bg_pattern_b.svg')",
       'cut-section': "url('/cut_bg.jpg')",
       'hero-section': "url('/hero.png')",
       'login-img': "url('/login_img.png')",
      }),
      screens:{
        '2xl': '1440px',
        'xl': '1440px',
		'md-max': {'max': '767px'},
		'md-tab': {'min': '768px', 'max': '1023px'},
      },
	  backgroundPosition:{
        'center-bottom': 'center bottom',
      },
	  borderWidth:{
		  '3' : '3px',
	  },
	  backgroundSize: {
        'x-full': '100% auto',
        'y-full': 'auto 100%',
      },
	  fontFamily:{
		  'sans': ['Rubik', 'sans-serif'],
		  'serif': ['Rubik', 'sans-serif'],
		  'mono': ['Rubik', 'sans-serif'],
		  'body': ['Rubik', 'sans-serif'],
		  'display': ['Rubik', 'sans-serif'],
	  },
      colors:{
		green:{
		  400: '#61A60E',
		  700: '#006937',
		},
		gray:{
		  200: '#F5F6F8',
		  600: '#222222',
		  700: '#231F20',
		},
		blue:{
		  700: '#002F87',
		},
		orange:{
		  700: '#FF5100',
		},
		red:{
		  700: '#8A2432',
	  	},
	  },
    }
  }
};