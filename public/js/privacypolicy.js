
jQuery(document).ready(function () {

    const url = "https://api.swishersweets.com/privacy_policy.php";
    const pp = jQuery("#privacy-policy-container");
    pp.load(url, function() {
        // remove brand logos image
        jQuery(this).find("img[src*=swisher-with-brand-logos]").remove();
        // scroll anchor (if any) into view
        let hash = location.hash.replace(/^#/, "");
        if (hash) {
            let t = jQuery(`a[name="${hash}"]`).offset().top - jQuery("#privacy-policy-container").offset().top + 100;
            jQuery('html,body').animate({scrollTop: t}, 500);
        }
    });
    
    
    const ca_url = "https://api.swishersweets.com/ca-supply-chains.php";
    const ca_pp = jQuery("#ca-supply-chain-container");
    
    let tester;
    
    ca_pp.load(ca_url, function() {
        // remove brand logos image
        jQuery(this).find("img[src*=swisher-with-brand-logos]").remove();
        // scroll anchor (if any) into view
        let hash = location.hash.replace(/^#/, "");
        if (hash) {
            let t = jQuery(`a[name="${hash}"]`).offset().top - jQuery("#ca-supply-chain-container").offset().top + 100;
            jQuery('html,body').animate({scrollTop: t}, 500);
        }
    });
    
    console.log(`privacypolicy.js`);
    console.log(pp)
    console.log(ca_pp)
    
});