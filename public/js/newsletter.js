// swisher email signup functions

$(function() {
    const apiEndpoint = "https://api.swishersweets.com/email_subscribe.php";
    const $emailDialog = $("#email-signup");
    const $emailForm = $("#email-signup-form");
    const $success = $(".success", $emailDialog);
    const $failed = $(".failed", $emailDialog);
    const $emailRef = $("#nl_email");
    const $modalEmail = $("#modal-email");
        
    // populate months menu
    const $bmonth = $("select[name=bmonth]", $emailForm);
    const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    months.forEach(function(m, i) {
        let opt = $("<option>").attr("value", i+1).text(m);
        $bmonth.append(opt);
    });

    // populate days menu
    const $bday = $("select[name=bday]", $emailForm);
    for (let i = 0; i < 31; i++) {
        let opt = $("<option>").attr("value", i+1).text(i+1);
        $bday.append(opt);
    }
                            
    // populate years menu
    const $byear = $("select[name=byear]", $emailForm);
    const thisYear = new Date().getFullYear();
    for (let i = thisYear-21; i >= thisYear-105; i--) {
        let opt = $("<option>").attr("value", i).text(i);
        $byear.append(opt);
    }

    try {
        // initialize the modal
        $emailDialog.dialog({
            modal: true,
            autoOpen: false,
            resizable: false,
            draggable: false,
            show: true,
            hide: true
        });
    } catch (e) {
        // console.log(e);
    }

    // handle click on a button/link to open the modal
    $("#newsletter-signup").on("submit", function(evt) {
        evt.preventDefault();
        $modalEmail.val($emailRef.val());
        $success.hide();
        $failed.hide();
        $emailForm.show();
        $emailDialog.dialog("open");
    });

    // close modal when backdrop clicked
    $("body").on("click", ".ui-widget-overlay", function(evt) {
        evt.preventDefault();
        $emailDialog.dialog("close");
    });
    $("body").on("click", "#email-signup-exit", function(evt) {
        evt.preventDefault();
        $emailDialog.dialog("close");
    });

    // handle form submit
    $emailForm.on("submit", function(evt) {
        evt.preventDefault();
        $emailForm.find("input[name=hs-submission_url]").val(document.location.href);
        $.post(apiEndpoint, $(this).serialize())
        .done(function(resp) {
            if (resp == "ok") {
                $emailForm.hide();
                $failed.hide();
                $success.show();
            } else {
                $success.hide();
                $failed.show();
            }
        })
        .fail(function(resp) {
            $success.hide();
            $failed.show();
        });
    });

    window.scrollTo(0,0);
});