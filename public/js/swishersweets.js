
const defaultMapZoom = 15;

var storesMap;  // the Google Maps object

var storesMarkers = [];  // map Marker objects currently displayed

var resetMap = true; // set to true to reset map center and bounds

var mapRedrawBlock = false; // true to prevent redraw on next idle event

function getFilters() {

    var $ = jQuery;

    var filters = [];

    // var filters = [

    //     'Rogue+Pouches',

    //     'Rogue+Lozenges',

    //     'Rogue+Tablets',

    //     'Rogue+Gum,'

    // ];

   

    $(".brand-blend-filter").each(function () {

        if ($(this).closest("li").find(".fusion-li-icon").hasClass("fa-check")) {

            filters.push($(this).data("tag"));

        }

    });



    $(".brand-blend-always").each(function () {

        filters.push($(this).data("tag"));

    });



    console.log("filters: " + filters.join(";"));



    if (filters.length) {

        return filters.join(";");

    }



    return "";

}

$(document).ready(function () {

    jQuery(function ($) {

        $("#store-search").on('keyup', function (e) {
            if (e.key === 'Enter' || e.keyCode === 13) { 
                $('#locator-search-button').click();
            }
        });

        $("#locator-search-button").on("click", function (e) {

            e.preventDefault();

            var latitude = "";

            var longitude = "";

            var filters = "";

            var storeAddress = $("#store-search").val() ? $("#store-search").val() : 'kayak';
            var address = $.trim(storeAddress);

            if (address == "") {
                var pos = window.storesMap.getCenter();
                latitude = pos.lat();
                longitude = pos.lng();
            }

            filters = getFilters();

            // console.log("searching", address, filters);

            // get nearby stores, create markers on map

            const apiUrl = "https:\/\/api.swishersweets.com\/store_locator.php";

            jQuery.getJSON(`https://api.swishersweets.com/store_locator.php?latitude=${latitude}&longitude=${longitude}&address=${address}&filters=Kayak+Apple;Kayak+Grape;Kayak+Mint;Kayak+Natural;Kayak+Peach;Kayak+Straight;Kayak+Wintergreen`)

                .then(function (data) {

                    if (typeof (data.stores) === "undefined") {

                        $("#locator-no-results").text("No stores found nearby.").show();

                    } else if (data.stores.length == 0 && filters.length > 0) {

                        $("#locator-no-results").text("No stores found nearby carrying selected brands\/blends.").show();

                    } else if (data.stores.length == 0) {

                        $("#locator-no-results").text("No stores found nearby.").show();

                    } else {

                        $("#locator-no-results").hide();

                        if ($(".locator-filters .panel-heading a").hasClass("active")) {

                            $(".locator-filters .panel-heading a").first().click();

                        }

                        $("#stores-map").trigger("locator.redraw_map", { results: data });
                    }
                });
        });

        $("#locator-filter-button").on("click", function (e) {
            e.preventDefault();
            $("#locator-search-button").click();
        });

    });



    // animate the brand/blend filters

    jQuery(function ($) {

        $('ul').has('.brand-blend-filter').on('click', 'li', function (e) {

            e.stopImmediatePropagation()

            var icon = $(this).closest('li').find('.fusion-li-icon');

            if (icon.hasClass('fa-check')) {

                icon.removeClass('fa-check').addClass('fa-fw');

            } else {

                icon.removeClass('fa-fw').addClass('fa-check');

            }

        });

    });



    jQuery(function ($) {

        var $container = $('#locator-list-pane');



        var renderList = function (markers) {

            var $virtualList = $("<ul></ul>");

            markers.map(function (m) {

                var directions = "https://google.com/maps/search/?" + $.param({
                    api: 1,
                    query: m.title + "," + m.address + "," + m.city + "," + m.state + " " + m.zipcode
                });

                var $item = $('<li class="locator-list-item" id="locator-list-item-'+ m.label.text + '"><div class="title" style="font-weight:bold">'+ m.label.text + '. ' + m.title + '</div><div class="address"><p>'+ m.address + '</p><p>'+ m.city + ', ' + m.state + '  ' + m.zipcode + '</p><div class="directions"><p class="text-yellow-700"><strong>'+ m.distance + ' miles</strong></p> <a href="'+ directions + '" target="_blank">Get directions</a></div></div></li>');                       

                $item.appendTo($virtualList);

            });

            $container.empty().append($virtualList);

        };

        $container.on('locator.refresh_list', function (e, data) {

            renderList(data.markers);

        });



        $container.on('locator.scroll_to', function (e, data) {

            var store = data.store;

            var $pane = $('#locator-list-pane');

            var paneTop = $pane.offset().top;

            var itemTop = $('#locator-list-item-' + store).offset().top;

            $('#locator-list-pane').animate(

                { scrollTop: (itemTop + $pane.scrollTop()) - paneTop },

                1000

            );

        });

    });

})





// create the initial map

function drawInitMap(position) {

    storesMap = new google.maps.Map(

        document.getElementById('stores-map'),

        {

            center: position,

            zoom: defaultMapZoom,

            gestureHandling: 'cooperative'

        }

    );



    storesMap.addListener('idle', function () {

        if (!mapRedrawBlock) {

            nearbyStores(this.getCenter());

        }



        mapRedrawBlock = false;

    });

}



// called by Google Maps API when Google JS loads

const urlParams = new URLSearchParams(window.location.search);



function initMap() {

    var zip = urlParams.get('zipcode');
    console.log(zip);

    if (zip) {
        $(document).ready(function () {
            setTimeout(function() {
                $("#store-search").val(zip);
                $("#locator-search-button").click()
            }, 500);
        });
    }else{
        $(document).ready(function () {
            setTimeout(function() {
                $("#locator-search-button").click()
            }, 500);
        });
    }

    navigator.geolocation.getCurrentPosition(

        // got location from browser

        function (pos) {

            drawInitMap(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));

        },

        // location blocked or error, use default (swisher hq)

        function () {

            drawInitMap(new google.maps.LatLng(30.3541296, -81.6487606));

        },

        {

            maximumAge: Infinity,

            enableHighAccuracy: false,

            timeout: 15000

        }

    );

}



// get nearby stores, create markers on map

function nearbyStores(position) {

    removeMarkers();

    const apiUrl = "https:\/\/api.swishersweets.com\/store_locator.php";

    jQuery.getJSON(`https://api.swishersweets.com/store_locator.php?latitude=${position.lat()}&longitude=${position.lng()}&filters=Kayak+Apple;Kayak+Grape;Kayak+Mint;Kayak+Natural;Kayak+Peach;Kayak+Straight;Kayak+Wintergreen`)

        .then(showMarkers);

}



// remove all markers from map

function removeMarkers() {

    storesMarkers.map(function (m) {

        m.setMap(null);

    });



    storesMarkers = [];

    jQuery('#locator-list-pane').trigger('locator.refresh_list', { markers: storesMarkers });

}



// show markers on the map given a list of markers from the locator API

function showMarkers(data) {

    // reset the map: remove all markers

    // recalc map bounds to contain all found stores

    var bounds = new google.maps.LatLngBounds();

    if (resetMap) {

        removeMarkers();

        data.stores.map(function (s, i) {

            // console.log(i, s.name, s.address, s.city, s.state, s.zipcode, s.latitude, s.longitude, s.distance);

            var pos = new google.maps.LatLng(s.latitude, s.longitude);

            bounds.extend(pos);

        });



        mapRedrawBlock = true;

        google.maps.event.addListenerOnce(storesMap, "bounds_changed", function (e) {

            if (storesMap.getZoom() > 15) {

                storesMap.setZoom(15);

            }

        });



        storesMap.fitBounds(bounds);



        resetMap = false;

    }



    bounds = storesMap.getBounds();



    // draw the markers that fit inside the map bounds

    data.stores.map(function (s, i) {

        var pos = new google.maps.LatLng(s.latitude, s.longitude);

        if (bounds.contains(pos)) {

            // console.log(i+1, s.name);

            var marker = new google.maps.Marker({

                position: pos,
                icon:  '/marker.png',
                label: {
                    text: (i + 1).toString(),
                    color: 'white'
                },

                title: s.name,

                map: storesMap,

                draggable: false,

                address: s.address,

                city: s.city,

                state: s.state,

                zipcode: s.zipcode,

                distance: s.distance

            });



            marker.addListener('click', function () {

                jQuery('#locator-list-pane').trigger('locator.scroll_to', { store: this.label.text });

            });



            storesMarkers.push(marker);

        }

    });



    jQuery('#locator-list-pane').trigger('locator.refresh_list', { markers: storesMarkers });

}



jQuery(function ($) {

    $('#stores-map').on('locator.redraw_map', function (e, data) {

        window.resetMap = true;

        showMarkers(data.results);

    });

});