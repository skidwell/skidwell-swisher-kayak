import Head from "next/head";
import BodyClass from '../components/BodyClass';

export default function Layout({ children }) {
  return (
    <>
      <Head>
        <title>Swisher Kayak</title>
        <link rel='icon' href='/favicon.ico' />
        <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js"></script>
      </Head>
      <BodyClass />
      <div>{children}</div>
    </>
  );
}
