import  { Component } from 'react';

class BodyClass extends Component {

    componentDidMount() {
        if (window.location.pathname == '/') {
            this.setBodyClass('home');
        } else if (window.location.pathname == '/agegate' || window.location.pathname == '/register' 
            || window.location.pathname == '/forgot-password' || window.location.pathname == '/change-password' 
            ) {
            this.setBodyClass('hide_header_footer');
        } 
    }

    setBodyClass(className) {
        // remove other classes
        document.body.className ='';

        // assign new class
        document.body.classList.add(className);
    }

    render() { 
        return null;
    }

}
 
export default BodyClass;