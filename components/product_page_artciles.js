import Link from "next/link";

export const productArticle = ({ products }) => {
  
  return (
		  <div className="product-list flex flex-wrap md-max:flex-col pb-20 md:-mx-8">
          {products.map((product, i) => (
            <Link href={`/product/${product.slug}`} key={i}>
              <div className="product-item md:text-center cursor-pointer md:w-1/2 lg:w-1/3 p-8 md-max:px-0 md-max:py-2 md-max:flex">
                <div className="product-image bg-gray-200 px-6 py-10 md-max:p-2 md-max:w-28">
                <img className="mx-auto" src={product.image[0].url} alt={product.title} /></div>
	  			<div className="product-content p-6 border-3 border-solid border-gray-200 md-max:flex-grow">
					<div className={ 'text-' + product.color + '-700 font-bold uppercase text-2xl md-max:text-lg'}>{product.title}</div>
					<div className="price font-semibold text-lg">${product.price}</div>
	  			</div>
              </div>
            </Link>
          ))}
      </div>
  );
}