import Link from "next/link";
import React, { useEffect, useState } from "react";
import $ from "jquery";
import Router , {useRouter}  from 'next/router';
import Navigations from "../components/navigations";
import MobileNavigations from "../components/mobile_navigations";
import getConfig from "next/config";

export default function Header({navigations, headerlogo}) {
  
  const router = useRouter()

  const openMenu = async () => {
    $("#mobile_menu").show();
  }
  const closeMenu = async () => {
    $("#mobile_menu").hide();
  }

  return (
<div className="relative bg-white" id="header">
  <div className="container mx-auto pl-4 pr-4">
    <div className="flex justify-between items-center py-6 md:justify-start md:space-x-10">
      <div className="flex justify-start lg:w-0 lg:flex-1 cursor-pointer">
        <a href="/">
          <img src={headerlogo.logo.url} alt={headerlogo.logo.url} width="153" height="72" />
        </a>
      </div>
      <div className="-mr-2 -my-2 md:hidden">
        <button type="button" onClick={openMenu} id="menu_open_btn" className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
          <span className="sr-only">Open menu</span>
          <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
          </svg>
        </button>
      </div>
      <Navigations navigations = {navigations}/>
    </div>
  </div>
  <div id="mobile_menu" className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden" style={{display: "none"}}>
    <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
      <div className="pt-5 pb-6 px-5">
        <div className="flex items-center justify-between">
          <div>
            <img src='/KayakOutdoors_logo.svg' alt='/KayakOutdoors_logo.svg' width="100" height="47" />
          </div>
          <div className="-mr-2">
            <button type="button" onClick={closeMenu} id="menu_close_btn" className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
              <span className="sr-only">Close menu</span>
              <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
              </svg>
            </button>
          </div>
        </div>
      </div>
      <div className="py-6 px-5 space-y-6">
        <MobileNavigations navigations = {navigations} />
      </div>
    </div>
  </div>
</div>
  );
}