import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useForm } from 'react-hook-form';

export default function Getme({footerNewsletter}) {

	const { register, handleSubmit, errors } = useForm();

    async function newsletterSubmit(formData) {
        if (typeof window !== 'undefined') {
            window.location.replace("/locator?zipcode=" + formData.zipcode);
        }
    }

	return (
		<div className="come_get_me pt-16 px-4 md-max:py-12 md-tab:pb-10">
			<div className="container mx-auto">
				<form onSubmit={handleSubmit(newsletterSubmit)}>
    				<div className="flex flex-nowrap md-max:flex-col justify-center items-center md-max:relative">
    					<div className="text-3xl md-max:text-xl font-bold uppercase pr-2 md-max:pb-4 md-max:text-center" dangerouslySetInnerHTML={{
                                    __html: footerNewsletter.content
                                  }} />
    					<div className="md-max:w-full">
    						<input className="text-3xl md-max:text-xl md-max:text-center w-36 md-max:w-full font-bold text-green-400 placeholder-green-400 border-t-0 border-r-0 border-l-0 focus:border-black focus:ring-transparent p-0 border-b-2 border-black uppercase" type="text" name="zipcode" ref={register({ required: "Please enter zip code" })} placeholder="zip code"/>
                                {errors.zipcode && <div className="text-left text-red-500">{errors.zipcode.message}</div>}
    					</div>
    					<div className="pl-2 md-max:absolute md-max:right-0 md-max:bottom-1">
    						<button type="submit">
    							<svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
    								<path d="M22.4709 20.2519L16.1244 14.5251C17.5279 12.7148 18.1889 10.4378 17.9729 8.15735C17.7569 5.87694 16.6802 3.7645 14.9618 2.2499C13.2434 0.735307 11.0125 -0.0676364 8.72302 0.00446751C6.43355 0.0765714 4.25758 1.0183 2.63789 2.63803C1.01821 4.25775 0.0765217 6.43376 0.00446206 8.72324C-0.0675976 11.0127 0.735384 13.2436 2.25 14.962C3.76463 16.6804 5.87707 17.7571 8.15747 17.973C10.4379 18.189 12.7149 17.5279 14.5251 16.1244L20.2519 22.4709C20.3941 22.6313 20.5676 22.7609 20.7618 22.8518C20.9559 22.9427 21.1666 22.9929 21.3809 22.9993C21.5952 23.0057 21.8085 22.9683 22.0078 22.8892C22.207 22.8102 22.388 22.6912 22.5396 22.5396C22.6912 22.388 22.8102 22.207 22.8892 22.0078C22.9683 21.8085 23.0057 21.5952 22.9993 21.3809C22.9929 21.1666 22.9427 20.9559 22.8518 20.7618C22.7609 20.5676 22.6313 20.3941 22.4709 20.2519ZM1.59321 9.0212C1.59321 7.55206 2.02885 6.11591 2.84506 4.89436C3.66127 3.67282 4.82137 2.72074 6.17867 2.15852C7.53597 1.59631 9.02951 1.4492 10.4704 1.73582C11.9113 2.02243 13.2349 2.72989 14.2737 3.76873C15.3125 4.80757 16.02 6.13113 16.3066 7.57205C16.5932 9.01296 16.4461 10.5065 15.8839 11.8638C15.3217 13.2211 14.3696 14.3812 13.1481 15.1974C11.9265 16.0137 10.4904 16.4493 9.02127 16.4493C7.05191 16.4471 5.16385 15.6637 3.7713 14.2712C2.37876 12.8786 1.59544 10.9906 1.59321 9.0212Z" fill="#231F20"/>
    							</svg>
    						</button>
    					</div>
    				</div>
				</form>
			</div>
		</div>
	);
}
