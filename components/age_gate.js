import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useRouter } from 'next/router'
import { useCookies } from "react-cookie"
import { useForm } from 'react-hook-form';

export default function AgeGate({agegate}) {
	
	const [cookies, setCookie] = useCookies(["agegate"]);
	const router = useRouter();
	const { register, handleSubmit, errors } = useForm();
	
	async function onSubmit(formData) {

		const currentPath = router.asPath ? router.asPath : '/';

		var dob = formData.year + "-" + formData.month + "-" + formData.day;
		var age = getAge(dob);
		console.log(age);
		console.log(currentPath);

		if(age>20){
			var error = false;
			$("#age_error").hide();
			
			setCookie("agegate_session", "yes", {
        path: "/",
        sameSite: true,
        secure: true
      });

      setCookie("agegate", age, {
        path: "/",
        maxAge: 1800, // Expires after 30min
        sameSite: true,
        secure: true
      });

			if(currentPath == "/locator"){
				location.reload();
			}else{
				// router.push(currentPath);
				window.location.href = currentPath
			}
		}else{
			$("#age_error").show();
			var error = true;
		}
	}

	function floatingLabel(e){
      var input = e.target
      $(input).parents(".floating_input").addClass("floated")
    }

    function floatingLabelOut(e){
      var input = e.target
      if(! $(input).val() ){
          $(input).parents(".floating_input").removeClass("floated")
      }
    }

	const getAge = birthDate => Math.floor((new Date() - new Date(birthDate).getTime()) / 3.15576e+10);

	const required = "This field is required";
    const monthMin = "Month must be greater than or equal 1";
    const monthMax = "Month must be less than or equal 12";

    const dayMin = "Day must be greater than or equal 1";
    const dayMax = "Day must be less than or equal 31";

    const yearMin = "Year must be greater than or equal 1930";
    const yearMax = "Year must be less than or equal 2021";
    // Error Component
    const errorMessage = error => {
      return <div className="text-left text-red-500">{error}</div>;
    };

return (
<div className="age_gate bg-age-backimage bg-no-repeat bg-bottom bg-cover py-20 px-4" style={{minHeight:'calc(100vh - 101px)'}} id="age_gate">
	<div className="bg-white max-w-3xl m-auto text-center p-6 md:p-12">
	  <div className="">
	    <div className="">
	      <div className="px-0 md:px-4">
	        <h3 className="text-lg font-medium font-bold text-black text-2xl md:text-3xl">{agegate.title}</h3>
	        <div className="text-gray-900 py-4 md:pt-5 md:pb-10 md:px-10" dangerouslySetInnerHTML={{
								__html: agegate.description
							  }}/>
	      </div>
	    </div>
	    <div className="">
	      <form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
	      	<div id="age_error" className="error_msg text-white bg-red-600 p-3" style={{display: "none"}}>You must be 21 years of age or older.</div>
	        <div className="overflow-hidden">
	          <div className="py-9 md-max:pt-4 text-left">
	            <div className="md:grid md:grid-cols-3 md:gap-4">
					<div className="md-max:pb-4 floating_input">
						<label htmlFor="first_name">Month (mm)</label>
						<input type="number" min="1" max="31" className="w-full pt-3 pb-3 age_month border-2 border-gray-300" name="month" id="age_month" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register({ required: true, min: 1, max: 12 })} />
						{errors.month &&  errors.month.type === "required" && errorMessage(required)}
		                {errors.month && errors.month.type === "min" && errorMessage(monthMin)}
		                {errors.month && errors.month.type === "max" && errorMessage(monthMax)}
					</div>
					<div className="md-max:pb-4 floating_input">
						<label htmlFor="last_name">Day (dd)</label>
						<input type="number" min="1" max="31" className="w-full pt-3 pb-3 age_day border-2 border-gray-300" name="day" id="age_day" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register({ required: true, min: 1, max: 31 })} />
						{errors.day &&  errors.day.type === "required" && errorMessage(required)}
               			{errors.day && errors.day.type === "min" && errorMessage(dayMin)}
               			{errors.day && errors.day.type === "max" && errorMessage(dayMax)}
					</div>
					<div className="floating_input">
						<label htmlFor="last_name">Year (yyyy)</label>
						<input type="number" min="1930" max="2021" className="w-full pt-3 pb-3 age_year border-2 border-gray-300" name="year" id="age_year" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register({ required: true, min: 1930, max: 2021 })} />
		               {errors.year &&  errors.year.type === "required" && errorMessage(required)}
		               {errors.year && errors.year.type === "min" && errorMessage(yearMin)}
		               {errors.year && errors.year.type === "max" && errorMessage(yearMax)}
					</div>
	            </div>
	          </div>
	          <div className="">
	            <button type="submit" className="w-1/2 mx-auto flex items-center uppercase justify-center whitespace-nowrap px-4 py-4 border border-transparent shadow-sm text-base font-bold text-white bg-green-700 hover:bg-black focus:outline-none">
	              Continue
	            </button>
	          </div>
	        </div>
	      </form>
	    </div>
	  </div>
	</div>
</div>
);
}
