export default function Warning({ warning }) {
return (
<div className="header_top_bar bg-black p-2 sticky top-0 z-10 warning_banner_container" id="warning-section">
        <div className="border text-white font-bold text-1xl md:text-3xl lg:text-4xl text-center p-6 warning_banner" dangerouslySetInnerHTML={{
                __html: warning.message
                }} />
  </div>
  );
}