import Router , {useRouter}  from 'next/router';
import Link from "next/link";
import React, { useEffect, useState } from "react";

const MobileNavigations = ({ navigations }) => {
  const router = useRouter()

  let navigationRows = [];  
  if(navigations.length>0){
      for (const [i, navigation] of Object.entries(navigations)) {
          if(navigation.slug == '/locator'){
            navigationRows.push(
              <a href={navigation.slug} key={i} className="text-base font-bold uppercase text-black whitespace-nowrap hover:text-green-700">{navigation.title}</a>
            );
          }else{
            navigationRows.push(
              <Link href={navigation.slug} key={i}><a className="text-base font-bold uppercase text-black whitespace-nowrap hover:text-green-700">{navigation.title}</a></Link>
            );
          }
      }
  }

  return (

  <div className="grid gap-y-4 gap-x-8">
	{navigationRows}
  <Link href="/"><a className="w-full flex items-center uppercase justify-center px-4 py-2 border border-transparent shadow-sm text-base font-bold text-white bg-green-700 hover:bg-black">does it dip?!</a></Link>
</div>

  )
}

export default MobileNavigations
