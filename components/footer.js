import Link from "next/link";

export default function Footer({ footerData, headerlogo, footerNewsletter, footerLinks }) {

return (
<nav id="footer" className="bg-gray-700 bg-main-pattern-b bg-x-full bg-no-repeat pt-10 md-tab:pt-16 md:pt-28">
    
    <form className="form transparent" id="newsletter-signup" action="#" method="post">
        <fieldset className="form-fieldset">
            <div className="form-field py-10">
                <div className="form-prefixPostfix wrap flex items-center">
                    <input id="newsletter_subscribe_button" className="button button--em mx-auto px-6 py-3 text-3xl font-semibold uppercase cursor-pointer rounded-md text-white bg-yellow-700 transition-colors duration-200 hover:bg-green-400" type="submit" value="Verify Age &amp; SignUP" />
                </div>
            </div>
        </fieldset>
    </form>

    <div id="email-signup">
        <div style={{ textAlign: "center"}}>
        
            <img id="email-signup-logo" src={headerlogo.logo.url} alt={headerlogo.logo.url} />
            
            {/* cdn 'assets/icons/close.svg' not working <img id="email-signup-exit" src="{{cdn 'assets/icons/close.svg'}}" title="close" /> */}
            <h3 id="email-signup-exit">x</h3>
        </div>
        
        <div className="success">
        <p>Email signup successful, thanks!</p>
        </div>
        
        <div className="failed">
        <p>Could not sign you up, please try again with the information on your government-issued photo ID.</p>
        </div>
        
        <form id="email-signup-form">
            <input type="hidden" name="ajax" value="1" />
            <input type="hidden" name="min_age" value="21" />

            <input type="hidden" name="hs-kayak" value="Yes" />
            
            <input type="hidden" name="hs-submission_url" value="" />
            
            <div className="column column-first" style={{ width: '50%' }}>
                <label>First name</label>
                <input type="text" name="firstname" required maxLength="50" autoComplete="off"/>
            </div>
            <div className="column column-last" style={{ width: '50%' }}>
                <label>Last name</label>
                <input type="text" name="lastname" required maxLength="50" autoComplete="off" />
            </div>
            
            <div className="column column-first column-last" style={{ width: '100%' }}>
                <label>Email address</label>
                <input type="email" name="email" id="modal-email" required autoComplete="off" />
            </div>
        
            <div className="column column-first" style={{ width: '33%' }}>
                <label>Birthday</label>
                <select name="bmonth" required>
                    <option value="">Month</option>
                </select>
            </div>
        
            <div className="column" style={{ width: '33%' }}>
                <label>&nbsp;</label>
                <select name="bday" required>
                <option value="">Day</option>
                </select>
            </div>
        
            <div className="column column-last" style={{ width: '33%' }}>
                <label>&nbsp;</label>
                <select name="byear" required>
                <option value="">Year</option>
                </select>
            </div>
        
            <div className="column column-first" style={{ width: '75%' }}>
                <label>Street address</label>
                <input type="text" name="address1" required maxLength="100" autoComplete="off" />
            </div>
            <div className="column column-last" style={{ width: '25%' }}>
                <label>Apt/box</label>
                <input type="text" name="address2" maxLength="100" autoComplete="off" />
            </div>
                
            <div className="column column-first" style={{ width: '50%' }}>
                <label>City</label>
                <input type="text" name="city" required maxLength="50" autoComplete="off" />
            </div>
                    
            <div className="column" style={{ width: '25%' }}>
                <label>State</label>
                <select name="state" required>
                <option value=""></option>
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="DC">District Of Columbia</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
                <option value="AS">American Samoa</option>
                <option value="GU">Guam</option>
                <option value="PR">Puerto Rico</option>
                <option value="VI">Virgin Islands</option>
                </select>
            </div>
        
            <div className="column column-last" style={{ width: '25%' }}>
                <label>ZIP Code</label>
                <input type="text" name="zipcode" required pattern="[0-9]{5}" maxLength="5" />
            </div>
        
            <div className="buttons">
                <button type="submit" id="email-signup-submit">SIGN UP</button>
            </div>
        </form>
    </div>
    <div className="container mx-auto p-4">
        <div className="flex flex-wrap overflow-hidden text-center md:text-left sm:-mx-1 md:-mx-px lg:-mx-2 xl:-mx-2">
            <div className="w-full order-5 md:order-first overflow-hidden md:my-px md:px-px md:w-full lg:my-2 lg:px-2 lg:w-2/6 pb-6">
                <img className="ml-auto mr-auto md:ml-0 md:mr-0" src={footerData.footer_logo.url} alt={footerData.footer_logo.url} width="112" height="53" />
				<div className="text-white mt-6" dangerouslySetInnerHTML={{
                                __html: footerData.footer_description
                              }} />
            </div>
            
            {footerLinks.map((footerLink, i) => (
                <div key={i} className="w-1/2 overflow-hidden md:my-px md:px-px md:w-1/4 lg:my-2 lg:px-2 lg:w-1/6 pb-6">
                    <h4 className="text-white uppercase font-bold">{footerLink.title}</h4>
                    <div dangerouslySetInnerHTML={{
                                __html: footerLink.links
                              }} />
                </div>
            ))}

        </div>
        <div className="pt-4 justify-center text-white py-2 border-t border-white border-solid" dangerouslySetInnerHTML={{
                                __html: footerData.copyright_text
                              }} />
        </div>

    <script src="js/newsletter.js"></script>
</nav>
);
}
