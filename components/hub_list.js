import Link from "next/link";

export const hubList = ( hub ) => {

  const date = new Date(hub.created_at)
  const month = date.toLocaleString('default', { month: 'long' })
  const year = date.getFullYear()
  const postDate = month + " " + year
  
  return (

      <div className="item md:flex md:items-center pb-10">
          <div className="blog-left md:w-1/2 md:pr-8 md-max:pb-6">
            <img src={hub.list_image.url} alt={hub.list_image.url} />
          </div>
          <div className="blog-right md:w-1/2">
            <div className="tags font-bold text-sm uppercase pb-1 text-gray-700"> From {hub.hub_category ? hub.hub_category.title : 'N/A'} | {postDate} </div>
            <div className="blog-title text-xl text-gray-700 uppercase font-bold">{hub.title}</div>
            <div className="blog-desc leading-7 text-gray-700 pt-4 pb-4 font-light">
              <div dangerouslySetInnerHTML={{
                __html: hub.short_description
                }} />
            </div>
            <Link href={`/hub-details/${hub.slug}`}><a className="text-orange-700 text-sm font-bold underline">Read Now</a></Link>
          </div>
        </div>

  );
}