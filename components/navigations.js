import Router , {useRouter}  from 'next/router';
import Link from "next/link";
import React, { useEffect, useState } from "react";

const Navigations = ({ navigations }) => {
  const router = useRouter()

  let navigationRows = [];  
  if(navigations.length>0){
      for (const [i, navigation] of Object.entries(navigations)) {
          if(navigation.slug == '/locator'){
            navigationRows.push(
              <a href={navigation.slug} key={i} className="text-base font-bold uppercase text-black whitespace-nowrap hover:text-green-700">{navigation.title}</a>
            );
          }else{
            navigationRows.push(
              <Link href={navigation.slug} key={i}><a className="text-base font-bold uppercase text-black whitespace-nowrap hover:text-green-700">{navigation.title}</a></Link>
            );
          }
      }
  }

  return (

  <nav className="hidden items-center md:flex space-x-6 lg:space-x-10">
    {navigationRows}
    <Link href="/"><a className="w-full flex items-center uppercase justify-center whitespace-nowrap px-4 py-2 border border-transparent shadow-sm text-base font-bold text-white bg-green-700 hover:bg-black">does it dip?!</a></Link>
  </nav>

  )
}

export default Navigations