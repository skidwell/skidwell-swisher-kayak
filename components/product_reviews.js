import Router , {useRouter}  from 'next/router';
import Link from "next/link";
import { useCookies } from "react-cookie"
import { useForm } from 'react-hook-form';

const ProductReviews = (reviews,productId,productTitle) => {

    const [cookies, setCookie] = useCookies(["customer_name","customer_email"])
    const customer_name = cookies.customer_name;
    const customer_email = cookies.customer_email;

    if(customer_email){
        $("#email").addClass("floated")
    }
    if(customer_name){
        $("#name").addClass("floated")
    }

    const { register, handleSubmit, errors } = useForm();

    async function onSubmit(formData) {

        const reviewInfo = {
            name: formData.name,
            email: formData.email,
            comment: formData.comment,
            rating: formData.rating,
            product: productId,
            published_at: null,
            _publicationState:'preview'
        }

        const add = await fetch(`/api/add-product-review`, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(reviewInfo)
        })
        const addResponse = await add.json()

        if(addResponse.error){
            $("#error_message").text(addResponse.message)
            $("#error_message").show()
            $("#success_message").hide()
        }else{
            if(addResponse.id){
                $("#error_message").hide()
                $("#success_message").show()
                $("textarea").val('');
                $("textarea").parents(".floating_input").removeClass("floated")
            }else{
                $("#error_message").text("Somthing went wrong. Please try again.")
                $("#error_message").show()
                $("#success_message").hide()
            }
        }

        // Save name & email in this browser for the next time.
        if(document.querySelector('.review_check:checked') !== null){
            setCookie("customer_name", reviewInfo.name, {
                path: "/",
                maxAge: 86400, // Expires after 24hr
                sameSite: true,
            })

            setCookie("customer_email", reviewInfo.email, {
                path: "/",
                maxAge: 86400, // Expires after 24hr
                sameSite: true,
            })
        }

    }

    const rating_one = '<svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>';

    const rating_two = '<svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>';

    const rating_three = '<svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>';

    const rating_four = '<svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>';

    const rating_five = '<svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg><svg class="mx-1 w-4 h-4 fill-current text-yellow-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>';

    const reviewsData = []
    if(reviews.length>0){
        for (const [i, review] of Object.entries(reviews)) {

            const date = new Date(review.created_at)
            const day = date.getDate()
            const month = date.toLocaleString('default', { month: 'long' })
            const year = date.getFullYear()
            const postDate = month + " " + day + ", " + year
            var rating = 'one'
            if(review.rating == 'one'){
                rating = rating_one
            }else if(review.rating == 'two'){
                rating = rating_two
            }else if(review.rating == 'three'){
                rating = rating_three
            }else if(review.rating == 'four'){
                rating = rating_four
            }else if(review.rating == 'five'){
                rating = rating_five
            }

            reviewsData.push(
                <div key={i} className={i == 0 ? 'comment-item flex py-8' : 'comment-item flex py-8 border-t border-gray-300'}>
                    <div>
                        <figure className="w-16 h-16 rounded-full overflow-hidden bg-gray-200"></figure>
                    </div>
                    <div className="comment-item-details flex-grow pl-8">
                        <div className="flex justify-between items-center">
                        	<div className="font-bold">{review.name}</div>
                            <div className="flex items-center" dangerouslySetInnerHTML={{
                                __html: rating
                              }} />
                        </div>
                        <div className="font-light mb-3 text-gray-500">{postDate}</div>
                            <div className="leading-7" dangerouslySetInnerHTML={{
                                __html: review.comment
                              }} />
                    </div>
                </div>
            );
        }

    }else{
        reviewsData.push(
                <div key="50">No reviews found.</div>
            );
    }

    function floatingLabel(e){
      var input = e.target
      $(input).parents(".floating_input").addClass("floated")
    }

    function floatingLabelOut(e){
      var input = e.target
      if(! $(input).val() ){
          $(input).parents(".floating_input").removeClass("floated")
      }
    }

    return (
        <div className="comment-section">
            <h3 className="text-2xl text-gray-700 font-bold uppercase mb-8">Reviews</h3>
            <div className="comment-items">
                {reviewsData}
            </div>
            <div className="font-bold text-lg mt-12 mb-2">Leave a review on “{productTitle}”</div>
            <div className="mb-8">Your email address will not be published. Required fields are marked *</div>
            <div id="success_message" className="bg-green-200 py-3 px-4 border-2 border-green-300 border-dashed" style={{display: "none"}}>Your review was submitted successfully!</div>
            <div id="error_message" className="py-3 px-4 text-left bg-red-100 border-2 border-red-300 border-dashed" style={{display: "none"}}></div>
            <form className="comment-form" onSubmit={handleSubmit(onSubmit)}>
                <div className="my-6 flex">
                    <div className="title">Your rating *</div>
                    <div className="star-rating leading-none">
                      <input type="radio" id="5-stars" name="rating" defaultValue="five" ref={register({ required: true })}/>
                      <label htmlFor="5-stars" className="star">&#9733;</label>
                      <input type="radio" id="4-stars" name="rating" defaultValue="four" ref={register({ required: true })}/>
                      <label htmlFor="4-stars" className="star">&#9733;</label>
                      <input type="radio" id="3-stars" name="rating" defaultValue="three" ref={register({ required: true })}/>
                      <label htmlFor="3-stars" className="star">&#9733;</label>
                      <input type="radio" id="2-stars" name="rating" defaultValue="two" ref={register({ required: true })}/>
                      <label htmlFor="2-stars" className="star">&#9733;</label>
                      <input type="radio" id="1-star" name="rating" defaultValue="one" ref={register({ required: true })}/>
                      <label htmlFor="1-star" className="star">&#9733;</label>
                    </div>
                    {errors.name && <div className="text-left text-red-500">Rating is required</div>}
                </div>
                <div className="my-6 floating_input">
                    <label>Your Review</label>
                    <textarea className="w-full block py-3 h-48 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" name="comment" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register}></textarea>
                </div>
                <div className="my-6 floating_input" id="name">
                    <label>Name *</label>
                  <input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" name="name" defaultValue={customer_name} onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register({ required: true })} />
                  {errors.name && <div className="text-left text-red-500">Please enter your name</div>}
                </div>
                <div className="my-6 floating_input" id="email">
                  <label>Email address *</label>
                  <input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" name="email" defaultValue={customer_email} onFocus={floatingLabel} onBlur={floatingLabelOut} 
                    ref={register({
                      required: "Please enter email address",
                      pattern: {
                        value: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                        message: "Please enter a valid email address"
                      }
                    })}
                  />
                  {errors.email && <div className="text-left text-red-500">{errors.email.message}</div>}
                </div>
                <div className="my-6"><label>
                  <input className="mr-2 review_check" type="checkbox" defaultValue="1"/> Save my name, email, and website in this browser for the next time I review.</label>
                </div>
                <div className="mt-6 text-center">
                  <button className="uppercase px-8 py-5 font-bold text-white bg-green-700 hover:bg-black focus:outline-none" type="submit">Submit review</button>
                </div>
            </form>
        </div>
    );
}

export default ProductReviews;
