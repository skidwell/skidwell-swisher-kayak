import Router , {useRouter}  from 'next/router';
import Link from "next/link";
import { useCookies } from "react-cookie"
import { useForm } from 'react-hook-form';

const Reviews = (reviews,hubId,hubTitle) => {

    const [cookies, setCookie] = useCookies(["customer_name","customer_email"])
    const customer_name = cookies.customer_name;
    const customer_email = cookies.customer_email;

    if(customer_email){
        $("#email").addClass("floated")
    }
    if(customer_name){
        $("#name").addClass("floated")
    }

    const { register, handleSubmit, errors } = useForm();

    async function onSubmit(formData) {

        const reviewInfo = {
            name: formData.name,
            email: formData.email,
            comment: formData.comment,
            hub: hubId,
            published_at: null,
            _publicationState:'preview'
        }

        console.log(reviewInfo)
        
        $("#review_success").hide();

         const add = await fetch(`/api/add-review`, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(reviewInfo)
        })
        const addResponse = await add.json()

        if(addResponse.id){
            $("#review_success").show();
            $("textarea").val('');
            $("textarea").parents(".floating_input").removeClass("floated")
        }

        // Save name & email in this browser for the next time.
        if(document.querySelector('.review_check:checked') !== null){
            setCookie("customer_name", reviewInfo.name, {
                path: "/",
                maxAge: 86400, // Expires after 24hr
                sameSite: true,
            })

            setCookie("customer_email", reviewInfo.email, {
                path: "/",
                maxAge: 86400, // Expires after 24hr
                sameSite: true,
            })
        }
    }

    const reviewsData = []
    if(reviews.length>0){
        for (const [i, review] of Object.entries(reviews)) {

            const date = new Date(review.created_at)
            const day = date.getDate()
            const month = date.toLocaleString('default', { month: 'long' })
            const year = date.getFullYear()
            const postDate = month + " " + day + ", " + year

            reviewsData.push(
                <div key={i} className={i == 0 ? 'comment-item flex py-8' : 'comment-item flex py-8 border-t border-gray-300'}>
                    <div>
                        <figure className="w-16 h-16 rounded-full bg-gray-200"></figure>
                    </div>
                    <div className="comment-item-details flex-grow pl-8">
                        <div className="font-bold">{review.name}</div>
                        <div className="font-light mb-3 text-gray-500">{postDate}</div>
                        <div className="leading-7" dangerouslySetInnerHTML={{
                                __html: review.comment
                              }} />
                    </div>
                </div>
            );
        }

    }else{
        reviewsData.push(
                <div key="50">No comments found.</div>
            );
    }

    function floatingLabel(e){
      var input = e.target
      $(input).parents(".floating_input").addClass("floated")
    }

    function floatingLabelOut(e){
      var input = e.target
      if(! $(input).val() ){
          $(input).parents(".floating_input").removeClass("floated")
      }
    }

    return (
        <div className="comment-section max-w-5xl mx-auto my-12">
            <h3 className="text-2xl text-gray-700 font-bold uppercase mb-8">Comments</h3>
            <div className="comment-items">
                {reviewsData}
            </div>
            <div className="font-bold text-lg mt-12 mb-2">Leave a comment on “{hubTitle}”</div>
            <div className="mb-8">Your email address will not be published. Required fields are marked *</div>
            <div className="bg-green-200 py-3 px-4 border-2 border-green-300 border-dashed" id="review_success" style={{display: "none"}}>Your comment was submitted successfully!</div>
            <form className="comment-form" onSubmit={handleSubmit(onSubmit)}>
                <div className="my-6 floating_input">
                    <label>Your Comment</label>
                  <textarea className="w-full block py-3 h-48 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" name="comment" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register}></textarea>
                </div>
                <div className="my-6 floating_input" id="name">
                    <label>Name *</label>
                  <input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" name="name" defaultValue={customer_name} onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register({ required: true })} />
                  {errors.name && <div className="text-left text-red-500">Please enter your name</div>}
                </div>
                <div className="my-6 floating_input" id="email">
                  <label>Email address *</label>
                  <input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" name="email" defaultValue={customer_email} onFocus={floatingLabel} onBlur={floatingLabelOut} 
                    ref={register({
                      required: "Please enter email address",
                      pattern: {
                        value: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                        message: "Please enter a valid email address"
                      }
                    })}
                  />
                  {errors.email && <div className="text-left text-red-500">{errors.email.message}</div>}
                </div>
                <div className="my-6"><label>
                  <input className="mr-2 review_check" type="checkbox" defaultValue="1"/> Save my name, email, and website in this browser for the next time I comment.</label>
                </div>
                <div className="mt-6 text-center">
                  <button className="uppercase px-8 py-5 font-bold text-white bg-green-700 hover:bg-black focus:outline-none" type="submit">Submit Comment</button>
                </div>
            </form>
        </div>
    );
}

export default Reviews;
