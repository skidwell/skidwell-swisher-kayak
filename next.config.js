
module.exports = {
    env: {
        API_URL: process.env.API_URL
    },
    publicRuntimeConfig: {
        API_URL: process.env.API_URL,
        API_ENDPOINT: process.env.API_ENDPOINT,
        GOOGLE_API_KEY: process.env.NEXT_PUBLIC_GOOGLE_API_KEY
    }
}
