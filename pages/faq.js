import Head from "next/head";
import Layout from "../components/layout";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";
import { server } from '../config';

const Faq = ({ faq, siteUrl }) => {

	const router = useRouter();

	const handleFaq = (e) =>  {
		var input = e.target;
		$(input).parents('.faq-items').toggleClass('open');
	}

	function makeBreadcrumbJobSchema() {
      return {
         "@context": "https://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "Home",
            "item": server
          },{
            "@type": "ListItem",
            "position": 2,
            "name": "Frequently Asked Questions"
          }]
      }
  }

  return (
    <Layout>
      <Head>
      <title>{faq.seo ? faq.seo.title_tag : 'FAQs'}</title>
      <meta name="title" content={faq.seo ? faq.seo.title_tag : ''}/>
      <meta name="description" content={faq.seo ? faq.seo.meta_description : ''}/>
      <link rel="canonical" href={siteUrl + '/faq'} />
      <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v1.8.0/dist/alpine.js" defer></script>
      <script
            type='application/ld+json'
            dangerouslySetInnerHTML={{ __html: JSON.stringify(makeBreadcrumbJobSchema()) }}
        />
      </Head>
		<div className="container mx-auto px-4">
			<div className="products-head pt-10 pb-10 md:pb-24 md-max:-m-4 md-max:px-4 bg-green-700 text-center bg-main-pattern-t bg-bottom bg-x-full bg-no-repeat">
				<div className="text-lg text-white uppercase">{faq.tagline}</div>
				<h1 id="page_title" className="text-3xl py-3 font-bold uppercase text-white">{faq.title}</h1>
				<div className="mx-auto text-white" dangerouslySetInnerHTML={{
                __html: faq.short_description
                }} />
			</div>
	  		<div className="max-w-5xl mx-auto">
				  <div className="faq_details py-4">
            <h2 className="py-8 text-2xl font-bold uppercase text-gray-700">General Questions</h2>
            
            {faq.faqs.map((row, i) => (
            <div key={i} className={i == 0 ? 'faq-items mb-8 open' : 'faq-items mb-8'}>
              <div className="faq-title cursor-pointer py-2 border-b border-gray-300 text-lg font-medium" onClick={handleFaq}>{row.question}</div>
              <div className="fa-description transition-all duration-1000" dangerouslySetInnerHTML={{
                                __html: row.answer
                              }} />
            </div>
            ))}
            <p>For any other questions, click Submit a Request and we’ll get back to you.</p>
          </div>
	 		</div>
		</div>
    </Layout>
  );
}


export async function getServerSideProps() {

  const {API_URL} = process.env
  const res = await fetch(`${API_URL}/faq`)
  const data  = await res.json()

  const siteUrl = server

  return {
    props: {
      faq: data,
      siteUrl
    }
  }
}

export default Faq