import React from 'react';
import { useForm } from 'react-hook-form';
import Head from "next/head";
import Layout from "../components/layout";
import getConfig from 'next/config'
import { useState } from 'react'
import { setCookie } from 'nookies'
import { parseCookies  } from 'nookies'
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import Link from "next/link";
import { server } from '../config';

const Login = ({ confirm, loginData, footerData, headerlogo, siteUrl }) => {

    const router = useRouter();
    const { register, handleSubmit, errors } = useForm();

    const jwt = parseCookies().jwt
    if(jwt){
        Router.push('/account')
    }

    async function onSubmit(formData) {
        const loginInfo = {
            identifier: formData.username,
            password: formData.password
        }

        const login = await fetch(`/api/login`, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(loginInfo)
        })
        const loginResponse = await login.json();

        if(loginResponse.error){
            const message = loginResponse.message[0].messages[0].message
            $(".error_message").text(message)
            $(".error_message").show()
        }else{
            if(loginResponse.jwt){
                $(".error_message").hide()
                setCookie(null, 'jwt', loginResponse.jwt , {
                    maxAge: 1 * 24 * 60 * 60, // Expires after 24hrs
                    path: '/',
                })
                setCookie(null, 'userInfo', JSON.stringify(loginResponse.user) , {
                    maxAge: 1 * 24 * 60 * 60, // Expires after 24hrs
                    path: '/',
                })
                Router.push('/account')
            }else{
                $(".error_message").text('Somthing went wrong.')
                $(".error_message").show()
            }
        }
    }

    function floatingLabel(e){
      var input = e.target
      $(input).parents(".floating_input").addClass("floated")
    }

    function floatingLabelOut(e){
      var input = e.target
      if(! $(input).val() ){
          $(input).parents(".floating_input").removeClass("floated")
      }
    }

	return (
        <Layout>
            <Head>
              <title>{loginData.seo ? loginData.seo.title_tag : 'Login'}</title>
              <meta name="title" content={loginData.seo ? loginData.seo.title_tag : ''}/>
              <meta name="description" content={loginData.seo ? loginData.seo.meta_description : ''}/>
              <link rel="canonical" href={siteUrl + '/login'} />
            </Head>
			<div className="login_register md:flex md:h-screen">
				<div style={{ backgroundImage: "url(" + loginData.image.url + ")" }} className="lr_left_image md-max:hidden md:bg-cover lg:w-1/2 md-tab:w-1/2 bg-login-img bg-no-repeat bg-center"></div>
				<div className="lr_right_section md:self-center md:justify-self-center md:flex-grow md:h-screen md:overflow-y-auto text-center">
					<div className="lg:w-4/6 lg:mx-auto p-8">
						<div className="logo mb-6 cursor-pointer">
                            <a href="/">
                                <img className="mx-auto" src={headerlogo.logo.url} alt={headerlogo.logo.url} width="112" height="53" />
                            </a>
                        </div>
						<h2 className="font-black my-8 text-6xl md-max:text-4xl text-gray-700">{loginData.title}</h2>
						<div className="my-8" dangerouslySetInnerHTML={{
                            __html: loginData.description
                            }} />
						<div className="tabs my-12">
							<Link href="/login"><a className="mx-8 font-bold text-lg underline uppercase">Login</a></Link>
							<Link href="/register"><a className="mx-8 uppercase text-lg">Register</a></Link>
						</div>
						<div className="login_tab">
							<div className="error_message py-3 px-4 text-left bg-red-100 border-2 border-red-300 border-dashed" style={{display: "none"}}>Username or Password invalid.</div>
                            <div className={ confirm == 'true' ? 'bg-green-200 py-3 px-4 border-2 border-green-300 border-dashed' : 'bg-green-200 py-3 px-4 border-2 border-green-300 border-dashed hidden' } id="success_message"> Your account was confirmed successfully!</div>
							<form className="form_login" onSubmit={handleSubmit(onSubmit)}>
								<div className="my-6 floating_input">
									<label>Username or email address *</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" name="username" ref={register({ required: true })}/>
                                    <div className="text-left text-red-500">
                                        {errors.username && 'Username or email address is required'}
                                    </div>
                                </div>
								<div className="my-6 floating_input">
									<label>Password *</label>
                                    <input name="password" onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="password" autoComplete="new-password" ref={register({ required: true })}/>
                                    <div className="text-left text-red-500">
                                        {errors.password && 'Password is required'}
                                    </div>
                                    </div>
								<div className="my-6 flex justify-between">
									<div><input type="checkbox" /> Remember me</div>
									<div>
                                    <a href="/forgot-password" className="text-orange-700 underline font-bold">Lost password?</a></div>
								</div>
								<div className="my-6"><button className="w-full uppercase px-2 py-4 font-bold text-white bg-green-700 hover:bg-black focus:outline-none" type="submit">Log in</button></div>
							</form>
						</div>
						<div className="text-sm mt-24" dangerouslySetInnerHTML={{
                            __html: footerData.footer_warning_text
                            }} />
					</div>
				</div>
			</div>
        </Layout>
	);
}

export async function getServerSideProps({ query: {confirm = false} }) {

    const {API_URL} = process.env

    const res_loginpage = await fetch(`${API_URL}/login-page`)
    const loginData  = await res_loginpage.json()

    const res_footer = await fetch(`${API_URL}/footer`)
    const footerData  = await res_footer.json()

    const res_headerlogo = await fetch(`${API_URL}/header-logo`)
    const headerlogo  = await res_headerlogo.json()

    const siteUrl = server

  return {
    props: {
        confirm: confirm,
        loginData,
        footerData,
        headerlogo,
        siteUrl
    }
  }
}


export default Login;