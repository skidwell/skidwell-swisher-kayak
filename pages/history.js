import Head from "next/head";
import Layout from "../components/layout";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import { server } from '../config';
import getConfig from "next/config";

const History = ({ history, siteUrl }) => {

	const router = useRouter();

  function makeBreadcrumbJobSchema() {
      return {
           "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [{
              "@type": "ListItem",
              "position": 1,
              "name": "Home",
              "item": siteUrl
            },{
              "@type": "ListItem",
              "position": 2,
              "name": history.title ? history.title : "History"
            }]
        }
    }

  return (
    <Layout>
      <Head>
        <title>{history.seo ? history.seo.title_tag : 'History'}</title>
        <meta name="title" content={history.seo ? history.seo.title_tag : ''} />
        <meta name="description" content={history.seo ? history.seo.meta_description : ''} />
        <link rel="canonical" href={siteUrl + '/history'} />
        <script
            type='application/ld+json'
            dangerouslySetInnerHTML={{ __html: JSON.stringify(makeBreadcrumbJobSchema()) }}
        />
      </Head>
	  	<div className="container mx-auto px-4">
  			<div className="products-head pt-10 pb-10 md:pb-24 md-max:-m-4 md-max:px-4 bg-green-700 text-center bg-main-pattern-t bg-bottom bg-x-full bg-no-repeat">
  				<div className="text-lg text-white uppercase">{history.tagline}</div>
  				<h1 id="page_title" className="text-3xl py-3 font-bold uppercase text-white">{history.title}</h1>
  				<div className="mx-auto text-white" dangerouslySetInnerHTML={{
                __html: history.description
                }} />
  			</div>

        <div className="product-details max-w-5xl py-12 mx-auto">
          <h3 className="text-2xl text-gray-700 font-bold uppercase pb-4">{history.ourstory.title}</h3>
          <div className="leading-8 text-gray-600" dangerouslySetInnerHTML={{
                __html: history.ourstory.description
                }} />
          <div className="md:flex pt-10 md:pt-16 mission-vision">
            {history.ourmission ? (
            <div className="md:w-1/2 md:pr-8 md-max:pb-10">
              <h4 className="text-lg text-gray-700 font-bold pb-4">{history.ourmission.title}</h4>
              <div className="leading-8 text-gray-600" dangerouslySetInnerHTML={{
                __html: history.ourmission.description
                }} />
            </div>
            ) : (
                ''
              )
            }
            {history.ourvision ? (
            <div className="md:w-1/2 md:pl-8">
              <h4 className="text-lg text-gray-700 font-bold pb-4">{history.ourvision.title}</h4>
              <div className="leading-8 text-gray-600" dangerouslySetInnerHTML={{
                __html: history.ourvision.description
                }} />
            </div>
            ) : (
                ''
              )
            }
          </div>

          {history.company ? (
          <div className="md:flex items-center pt-10 md:pt-16">
            <div className="md:w-1/2 md:pr-8 md-max:pb-10"><img src={history.company.image.url} alt={history.company.image.url} /></div>
            <div className="md:w-1/2 md:pl-8">
              <h4 className="text-lg text-gray-700 font-bold pb-4">{history.company.title}</h4>
              <div className="leading-8 text-gray-600" dangerouslySetInnerHTML={{
                __html: history.company.description
                }} />
            </div>
          </div>
          ) : (
              ''
              )
          }
          
          {history.timeline ? (
          <div className="md:flex pt-10 md:pt-16">
            <div className="md-max:pb-10">
              <h4 className="text-lg text-gray-700 font-bold pb-4">{history.timeline.title}</h4>
              <div className="leading-8 text-gray-600" dangerouslySetInnerHTML={{
                __html: history.timeline.description
                }} />
            </div>
          </div>
          ) : (
              ''
            )
          }
        </div>
  			
	  	</div>
    </Layout>
  );
}

export async function getServerSideProps() {

  const {API_URL} = process.env
  const res = await fetch(`${API_URL}/history`)
  const history  = await res.json()

  const siteUrl = server

  return {
    props: {
      history,
      siteUrl
    }
  }

}

export default History