import Head from "next/head";
import Layout from "../components/layout";
import Getme from "../components/getme";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";
import {categoryProducts} from "../components/category_products"
import Link from "next/link";
import { server } from '../config';
import getConfig from "next/config";

const Products = ({ products, categories, hubs, categorySlug, footerNewsletter, productBannerData, categoryPageContent, siteUrl }) => {

  const router = useRouter();

  function makeBreadcrumbJobSchema() {
      return {
           "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [{
              "@type": "ListItem",
              "position": 1,
              "name": "Home",
              "item": siteUrl
            },{
              "@type": "ListItem",
              "position": 2,
              "name": "Product landing page"
            }]
        }
    }
  
  return (

    <Layout>
        <Head>
          <title>{categoryPageContent.seo ? categoryPageContent.seo.title_tag : 'Products'}</title>
          <meta name="title" content={categoryPageContent.seo ? categoryPageContent.seo.title_tag : ''}/>
          <meta name="description" content={categoryPageContent.seo ? categoryPageContent.seo.meta_description : ''}/>
          <link rel="canonical" href={siteUrl + '/products'} />
          <script
              type='application/ld+json'
              dangerouslySetInnerHTML={{ __html: JSON.stringify(makeBreadcrumbJobSchema()) }}
          />
        </Head>
        <div className="product-section" id="product-section">
	  		<div className="container mx-auto px-4">
				<div className="products-head pt-10 pb-10 md:pb-24 md-max:-m-4 md-max:px-4 bg-green-700 text-center bg-main-pattern-t bg-bottom bg-x-full bg-no-repeat">
					<div className="text-lg text-white uppercase">{productBannerData.tagline}</div>
					<h1 id="page_title" className="text-3xl py-3 font-bold uppercase text-white">{productBannerData.title}</h1>
					<div className="mx-auto text-white" dangerouslySetInnerHTML={{
              __html: productBannerData.description
              }} />
				</div>	  	
				<div className="product_filters text-center py-10">
				  <ul className="text-center">
  					<li className="inline-block px-4 font-light">
              <Link href="/products">
            		<a className="text-orange-700 font-bold mr-2 border-b-2 border-orange-700" id="filter_all">View All Products</a>
              </Link>
            </li>
            {categories.map((category, i) => (
              <li key={i} className="border-l-2 border-solid border-gray-700 inline-block px-4 font-light">
                  <Link href={'/category/'+category.slug}>
                  <a id={'product_'+category.slug}>{category.title}</a></Link>
              </li>
            ))}
				  </ul>
				</div>
				<div className="product-details max-w-5xl mx-auto">
				  {categories.map((category, i) => (
          <div key={i} id={ 'category_' + category.slug }>
  					<div className="product_items">
  					  <div className="category_detsils pb-8">
  						<h4 className="text-3xl font-bold uppercase pb-4 text-gray-700">{ category.title }</h4>
  						<div className="text-gray-700 pb-6" dangerouslySetInnerHTML={{
  							__html: category.description
  						  }} />
                <Link href="/products">
  						    <a className="text-orange-700 font-bold underline" >View All { category.title }</a>
                </Link>
  					  </div>
  					  {categoryProducts(category)}
  					</div>
            <div id={ 'bottom_content_'  + category.slug } style={{display: 'none'}} className="bottom-content text-center pb-12">
              <div dangerouslySetInnerHTML={{
                __html: category.bottom_text
                }} />
            </div>
          </div>
				  ))}
				</div>
        <div className="text-center pb-12 bottom-content" id="bottom_content_all">
          <div dangerouslySetInnerHTML={{
              __html: categoryPageContent.description
              }} />
        </div>
	  		</div>
			<div className="blog_news px-4">
                <div className="mb-0 pt-20 pb-20 md:pb-96 md-tab:pb-56 md-tab:-mb-40 lg:-mb-72 text-center bg-gray-700 -mx-4 md-max:px-4 md:bg-main-pattern-t md:bg-x-full md:bg-bottom md:bg-no-repeat">
                    <h3 className="text-white pb-4 uppercase text-lg">{categoryPageContent.tagline}</h3>
                    <h2 className="text-3xl md-max:text-xl text-white font-bold uppercase pr-2 md-max:pb-4 md-max:text-center">{categoryPageContent.title}</h2>
                </div>
				<div className="container mx-auto">
					<div className="blog_news_items md-max:pb-12 md-max:bg-gray-700 md-max:-mx-4 md-max:px-4">
						<div className="container mx-auto">
							<div className="flex flex-wrap md-max:flex-col">
								{hubs.map((hub, i) => (
								<div key={i} className={i%2 == 0 ? 'blog_news_item md:w-1/2 md:pr-8 md-tab:pr-4 md-max:text-center pb-12' : 'blog_news_item md:w-1/2 md:pl-8 md-tab:pl-4 md-max:text-center pb-12'}>
									<figure><img className="w-full" src={hub.list_image.url} alt={hub.list_image.url} /></figure>
									<div className="">From {hub.hub_category.title}</div>
									<div className="text-black font-bold uppercase text-2xl md-max:text-xl md-max:text-white">{hub.title}</div>
									<Link href={`/hub-details/${hub.slug}`}>
                  <a className="underline text-orange-700 text-sm font-bold">Read Now</a>
								  </Link>
                </div>
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
      <Getme footerNewsletter={footerNewsletter} />
      </div>
      </Layout>
  )
}

export async function getServerSideProps({ query: {category=null} }) {

  const {API_URL} = process.env
  
  const res_products = await fetch(`${API_URL}/products?_sort=id:ASC`)
  const products  = await res_products.json()

  const res_categories = await fetch(`${API_URL}/categories`)
  const categories  = await res_categories.json()

  const res_hubs = await fetch(`${API_URL}/hubs?_limit=4&_sort=id:ASC`)
  const hubs  = await res_hubs.json()

  const res_footernewsletter = await fetch(`${API_URL}/footer-newsletter`)
  const footerNewsletter  = await res_footernewsletter.json()

  const res_productbanner = await fetch(`${API_URL}/product-page-banner`)
  const productBannerData  = await res_productbanner.json()

  const res_categorypagecontent = await fetch(`${API_URL}/category-page-content`)
  const categoryPageContent  = await res_categorypagecontent.json()

  const siteUrl = server

  return {
    props: {
      products: products,
      categories: categories,
      hubs: hubs,
      categorySlug: category,
      footerNewsletter,
      productBannerData,
      categoryPageContent,
      siteUrl
    }
  }

}

export default Products