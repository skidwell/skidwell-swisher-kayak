import Head from "next/head";
import Layout from "../components/layout";
import Getme from "../components/getme";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";
import Link from "next/link";
import { useForm } from 'react-hook-form';
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import getConfig from "next/config";
import { server } from '../config';

const Home = ({ products, hubs, job, homeData, homeProducts, homeCategory, ourhistory, ourpassion, footerNewsletter, homeSliders, homeCategories, siteUrl }) => {

    const router = useRouter();

    function makeJobSchema() {
        return {
            "@context": "http://schema.org",
            "@type": "Corporation",
            "@id": job.url,
            "name": job.name,
            "description": job.description,
            "logo": job.logo.url,
            "url": job.url,
            "sameAs": ["https://www.instagram.com/kayak.outdoors/"],
            "address": {
                "@type": "PostalAddress",
                "streetAddress": job.streetAddress,
                "addressLocality": job.addressLocality,
                "postalCode": job.postalCode,
                "addressCountry": job.addressCountry,
            }
        }
    }

    const { register, handleSubmit, errors } = useForm();

    async function homeNewsletterSubmit(formData) {
        if (typeof window !== 'undefined') {
            window.location.replace("/locator?zipcode=" + formData.zipcode);
        }
    }

    const videoScript = `
        $(function() {
            $('.popup-youtube, .popup-vimeo').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false
            });
        });
        `;

    return (
      <Layout>
        <Head>
            <title>{homeData.seo ? homeData.seo.title_tag : 'Home'}</title>
            <meta name="title" content={homeData.seo ? homeData.seo.title_tag : ''}/>
            <meta name="description" content={homeData.seo ? homeData.seo.meta_description : ''}/>
            <link rel="canonical" href={siteUrl} />
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
            <script
                type='application/ld+json'
                dangerouslySetInnerHTML={{ __html: JSON.stringify(makeJobSchema()) }}
            />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
            <script dangerouslySetInnerHTML={{ __html: videoScript }} type="text/javascript" />
        </Head>
        <div className="relative md:-mt-8">
			<div className="relative overflow-hidden">
			  <div className="main_slider main-hero-section container mx-auto md:px-4">
				<AliceCarousel autoPlayInterval="3000" infinite="true">
                {homeSliders.map((slider, i) => (
                  <div key={i} className="sliderimg md:pt-8">
                      <div className="bg-hero-section bg-no-repeat bg-center-bottom bg-y-full md:bg-x-full md-max:px-4">
    					  <div className="flex md-max:flex-col md-max:text-center">
    						  <div className="hero_boys md:-mt-8 md-tab:-mt-5 md-max:order-2">
                              <img src={slider.image.url} alt={slider.image.url} /></div>
    						  <div className="self-center pb-10 md-tab:pb-20 md:pb-44 md-max:pt-10 md-max:order-1">
    							  <div className="font-medium text-white text-lg uppercase">{slider.tagline}</div>
    							  <h1 className="slider-title font-medium text-white font-black py-3 uppercase" dangerouslySetInnerHTML={{
                                            __html: slider.title
                                          }} />
                                <div className="slider_action">

                                    {slider.first_button_label ? (
                                        slider.first_button_label && slider.is_first_button_video == true ? (
                                            <a href={slider.first_button_link} className="popup-youtube bg-transparent hover:bg-white w-56 p-3 font-bold text-white hover:text-black uppercase border-2 border-white border-solid"> <img className="inline-block mr-1" src="/play.png" alt="/play.png" /> {slider.first_button_label}</a>
                                        ) : (
                                            <button className="bg-transparent hover:bg-white w-56 p-3 font-bold text-white hover:text-black uppercase border-2 border-white border-solid" onClick={() => router.push(slider.first_button_link)}>{slider.first_button_label}</button>
                                        )
                                    ) : ""
                                    }

                                    {slider.second_button_label ? (
                                            slider.is_second_button_video == true ? (
                                                <a href={slider.second_button_link} className="popup-youtube bg-transparent hover:bg-white w-56 p-3 font-bold text-white hover:text-black uppercase border-2 border-white border-solid"> <img className="inline-block mr-1" src="/play.png" alt="/play.png" /> {slider.second_button_label}</a>
                                            ) : (
                                                <button className="bg-transparent hover:bg-white w-56 p-3 font-bold text-white hover:text-black uppercase border-2 border-white border-solid" onClick={() => router.push(slider.second_button_link)}>{slider.second_button_label}</button>
                                            )
                                        ) : ""
                                    }

    						  </div>
                              </div>
    					  </div>
    					  <div className="absolute left-0 right-0 -bottom-4 md-max:-left-4 md-max:-right-4 md-max:bottom-0">
                          <img className="w-full" src="/bg_pattern_t.svg" alt="/bg_pattern_t.svg" /></div>
    				  </div>
                  </div>
                ))}
                </AliceCarousel>
			  </div>
          </div>
	  	  <div className="slider-top-text flex md-tab:text-center">
            <div className="stt-item md-max:w-6/12 md-max:text-center md-tab:w-1/3">
	  			<div className="stt-subtitle text-white font-bold uppercase text-sm md-max:text-xs">Special Access</div>
                <div className="stt-title text-white font-bold uppercase text-2xl py-1 md-max:text-xl">Your Account</div>
                <a href="/account" className="stt-link underline text-orange-700 text-sm font-bold md-max:text-xs">Login Now</a>  	
	  		</div>
            <div className="stt-item pl-12 md-tab:pl-0 md-max:pl-0 md-max:w-6/12 md-max:text-center md-tab:w-1/3">
	  			<div className="stt-subtitle text-white font-bold uppercase text-sm md-max:text-xs">Find Us</div>
                <div className="stt-title text-white font-bold uppercase text-2xl py-1 md-max:text-xl">Store Locator</div>
                <a href="/locator" className="stt-link underline text-orange-700 text-sm font-bold md-max:text-xs">Search Now</a>
	  		</div>
            <div className="stt-item pl-12 md-tab:pl-0 md-max:hidden md-tab:w-1/3">
	  			<div className="stt-subtitle text-white font-bold uppercase text-sm md-max:text-xs">Champion's Corner</div>
                <div className="stt-title text-white font-bold uppercase text-2xl py-1 md-max:text-xl">The Blog</div>
                <a href="/hub/blog" className="stt-link underline text-orange-700 text-sm font-bold md-max:text-xs">Read Now</a>
	  		</div>
	  	  </div>
	    </div>
        <div className="one_for_me pt-20 relative">
            <div className="container mx-auto">
                <h2 className="text-gray-700 text-center text-4xl uppercase font-bold pb-4">{homeProducts.title}</h2>
                <div className="text-gray-700 text-center pb-4" dangerouslySetInnerHTML={{
                                __html: homeProducts.description
                              }} />
	  			<div className="text-center pb-16"><Link href="/products"><a className="uppercase text-orange-700 font-bold">explore all products &nbsp; <i className="fa fa-long-arrow-right" aria-hidden="true">&nbsp;</i></a></Link></div>
                <div className="flex -mb-20 flex-nowrap md-max:px-4 overflow-x-auto md:justify-center">
                    {products.map((product, i) => (
                    <Link href={`/product/${product.slug}`} key={i}>
                        <div className="max-w-xs cursor-pointer one_me_item bg-gradient-to-b mr-4 md:mr-8 px-4 md:px-8 pt-2 pb-12 from-white to-gray-100 text-center" style={{ minWidth : '300px' }}>
                            <figure className="pb-8"><img width="285" height="285" className="m-auto" src={product.image[0].url} alt={product.title} /></figure>
                            <div className={ 'text-' + product.color + '-700 font-bold uppercase text-xl'}>{product.title}</div>
                            <div className="text-black text-sm py-8" dangerouslySetInnerHTML={{
                            __html: product.short_description
                          }} />
                            <div className="text-black text-sm">
                                {product.category.title}
                            </div>
                        </div>
                    </Link>
                    ))}
                </div>
            </div>
	    </div>
        <div className="blog_news pt-40 px-4 bg-gray-700 mb-10 md:mb-60 bg-main-pattern-t bg-bottom bg-x-full bg-no-repeat">
			<div className="container mx-auto">
	  			<form onSubmit={handleSubmit(homeNewsletterSubmit)}>
                <div className="flex flex-nowrap md-max:flex-col justify-center items-center mb-20 md-max:relative">
	  				<p className="text-3xl md-max:text-xl text-white font-bold uppercase pr-2 md-max:pb-4 md-max:text-center">{homeData.zipcode_text}</p>
	  				<div className="md-max:w-full">
                        <input className="text-3xl md-max:text-xl md-max:text-center w-36 md-max:w-full font-bold bg-gray-700 text-orange-700 placeholder-orange-700 border-t-0 border-r-0 border-l-0 focus:border-white focus:ring-transparent p-0 border-b-2 border-white uppercase" type="text" name="zipcode" ref={register({ required: "Please enter zipcode" })} placeholder="zip code"/>
                            {errors.zipcode && <div className="text-left text-red-500">{errors.zipcode.message}</div>}
                        </div>
	  				<div className="pl-2 md-max:absolute md-max:right-0 md-max:bottom-1">
	  				  <button type="submit">
                        <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M22.4709 20.2519L16.1244 14.5251C17.5279 12.7148 18.1889 10.4378 17.9729 8.15735C17.7569 5.87694 16.6802 3.7645 14.9618 2.2499C13.2434 0.735307 11.0125 -0.0676364 8.72302 0.00446751C6.43355 0.0765714 4.25758 1.0183 2.63789 2.63803C1.01821 4.25775 0.0765217 6.43376 0.00446206 8.72324C-0.0675976 11.0127 0.735384 13.2436 2.25 14.962C3.76463 16.6804 5.87707 17.7571 8.15747 17.973C10.4379 18.189 12.7149 17.5279 14.5251 16.1244L20.2519 22.4709C20.3941 22.6313 20.5676 22.7609 20.7618 22.8518C20.9559 22.9427 21.1666 22.9929 21.3809 22.9993C21.5952 23.0057 21.8085 22.9683 22.0078 22.8892C22.207 22.8102 22.388 22.6912 22.5396 22.5396C22.6912 22.388 22.8102 22.207 22.8892 22.0078C22.9683 21.8085 23.0057 21.5952 22.9993 21.3809C22.9929 21.1666 22.9427 20.9559 22.8518 20.7618C22.7609 20.5676 22.6313 20.3941 22.4709 20.2519ZM1.59321 9.0212C1.59321 7.55206 2.02885 6.11591 2.84506 4.89436C3.66127 3.67282 4.82137 2.72074 6.17867 2.15852C7.53597 1.59631 9.02951 1.4492 10.4704 1.73582C11.9113 2.02243 13.2349 2.72989 14.2737 3.76873C15.3125 4.80757 16.02 6.13113 16.3066 7.57205C16.5932 9.01296 16.4461 10.5065 15.8839 11.8638C15.3217 13.2211 14.3696 14.3812 13.1481 15.1974C11.9265 16.0137 10.4904 16.4493 9.02127 16.4493C7.05191 16.4471 5.16385 15.6637 3.7713 14.2712C2.37876 12.8786 1.59544 10.9906 1.59321 9.0212Z" fill="#fff"/>
                        </svg>
                       </button>
                    </div>
	  			</div>
                </form>
				<div className="blog_news_items pb-32 md-max:pb-12">
                    <div className="container mx-auto">
                        <div className="flex md-max:flex-col">
                            {hubs.map((hub, i) => (
                            <Link key={i} href={`/hub-details/${hub.slug}`}>
                            <div className={i > 0 ? 'blog_news_item md:w-6/12 md:pl-8 md:-mb-96 md-max:text-center cursor-pointer' : 'blog_news_item md:w-6/12 md:pr-8 md:-mb-96 md-max:text-center md-max:pb-12 cursor-pointer'}>
                                <figure><img className="w-full" src={hub.list_image.url} alt={hub.list_image.url} /></figure>
                                <div className="text-black font-bold uppercase text-sm md-max:text-xs pt-6 md-max:text-white">From {hub.hub_category.title}</div>
                                <div className="text-black font-bold uppercase text-2xl md-max:text-xl md-max:text-white">{hub.title}</div>
                                <a className="underline text-orange-700 text-sm font-bold">Read Now</a>
                            </div>
                            </Link>
                            ))}
                        </div>
                    </div>
				</div>
	  		</div>
	    </div>
        <div className="always_ready px-4">
			<div className="container mx-auto">
				<div className="flex md-max:flex-col items-center md-max:pb-10">
					<div className="md:order-2 md:w-6/12 md-max:pb-10"><img src={ourhistory.image.url} alt={ourhistory.image.url} /></div>
					<div className="md:order-1 md:w-6/12 lg:pr-56 md-max:text-center">
						<h3 className="font-medium text-gray-700 text-lg uppercase md-max:text-base">{ourhistory.tagline}</h3>
						<h2 className="font-medium text-gray-700 font-black text-7xl md-tab:text-5xl md-max:text-5xl py-3 uppercase">{ourhistory.title}</h2>
						<div className="text-gray-700 pb-3 md-max:pb-8" dangerouslySetInnerHTML={{
                            __html: ourhistory.description
                          }} />
						<button onClick={() => router.push(ourhistory.button_link)} className="bg-red-700 hover:bg-black w-56 p-3 text-white uppercase font-bold">{ourhistory.button_label}</button>
					</div>
				</div>
	 		</div>
	    </div>
        <div className="cut_section pb-32">
			<div className="bg-cut-section bg-no-repeat bg-center bg-top bg-cover">
				<div className="bg-main-pattern-b bg-x-full bg-no-repeat pt-20 md:pt-40 px-4">
					<div className="container mx-auto">
                        <h2 className="text-white text-center text-4xl md-max:text-2xl uppercase font-bold pb-4">{homeCategory.title}</h2>
                        <div className="text-white text-center pb-16" dangerouslySetInnerHTML={{
                            __html: homeCategory.description
                          }} />
                        <div className="flex flex-nowrap justify-center items-center">
                            { homeCategories.map((category, i) => (
                            <div key={i} className={i == 0 ? 'cut_section_item bg-gradient-to-b mr-8 md-max:mr-2 px-8 pt-2 pb-12 md-max:pb-4 -mb-20 from-white to-gray-100 text-center md-max:px-2' : 'cut_section_item bg-gradient-to-b ml-8 md-max:ml-2 px-8 pt-2 pb-12 md-max:pb-4 -mb-20 from-white to-gray-100 text-center md-max:px-2'}>
                                <figure><img src={category.image.url} alt={category.image.url} /></figure>
                                <div className="text-black font-bold uppercase text-sm md-max:text-xs">{category.tagline}</div>
                                <div className="text-black font-bold uppercase text-2xl md-max:text-xl">{category.title}</div>
                                <Link href={'/category/'+category.slug}><a className="underline text-orange-700 text-sm font-bold uppercase md-max:text-xs">view all</a></Link>
                            </div>
                            ))}
                        </div>
					</div>
				</div>
			</div>
	    </div>
        <div className="seeking_adventure bg-main-pattern-b bg-x-full bg-no-repeat bg-blue-700 px-4 md-max:pb-10">
			<div className="container mx-auto">
				<div className="flex md-max:flex-col items-center">
					<div className="md:w-6/12 md-max:-mt-6"><img src={ourpassion.image.url} alt={ourpassion.title} /></div>
					<div className="md:w-6/12 md-max:text-center pt-10">
						<h3 className="font-medium text-white text-lg md-max:text-base uppercase">{ourpassion.tagline}</h3>
						<h2 className="font-medium text-white font-black text-7xl md-tab:text-5xl md-max:text-5xl py-3 md-max:pb-8 uppercase">{ourpassion.title}</h2>
						<div className="text-white pb-3 md-max:pb-8" dangerouslySetInnerHTML={{
                            __html: ourpassion.description
                          }}
                        />
						<button onClick={() => router.push(ourpassion.button_url)} className="bg-orange-700 hover:bg-black w-74 p-3 text-white uppercase font-bold">{ourpassion.button_label}</button>
					</div>
				</div>
	 		</div>
	    </div>
        <Getme footerNewsletter={footerNewsletter} />
      </Layout>
  )
}

export async function getServerSideProps() {

    const {API_URL} = process.env

    const res_products = await fetch(`${API_URL}/products?_limit=3&_sort=id:ASC`)
    const products  = await res_products.json()

    const res_hubs = await fetch(`${API_URL}/hubs?Is_show_on_home=true&_limit=2&_sort=id:ASC`)
    const hubs  = await res_hubs.json()

    const res_ld = await fetch(`${API_URL}/json-ld`)
    const jsonld  = await res_ld.json()

    const res_home = await fetch(`${API_URL}/home-page`)
    const homeData  = await res_home.json()

    const res_home_products = await fetch(`${API_URL}/home-products`)
    const homeProducts  = await res_home_products.json()

    const res_home_category = await fetch(`${API_URL}/home-category`)
    const homeCategory  = await res_home_category.json()

    const res_ourhistory = await fetch(`${API_URL}/our-history`)
    const ourhistory  = await res_ourhistory.json()

    const res_ourpassion = await fetch(`${API_URL}/our-passion`)
    const ourpassion  = await res_ourpassion.json()

    const res_footernewsletter = await fetch(`${API_URL}/footer-newsletter`)
    const footerNewsletter  = await res_footernewsletter.json()

    const res_slider = await fetch(`${API_URL}/home-page-sliders`)
    const homeSliders  = await res_slider.json()

    const res_home_categories = await fetch(`${API_URL}/categories?is_show_on_home=true&_limit=2&_sort=id:ASC`)
    const homeCategories  = await res_home_categories.json()

    const siteUrl = server

  return {
    props: {
      products: products,
      hubs: hubs,
      job: jsonld,
      homeData,
      homeProducts,
      homeCategory,
      ourhistory,
      ourpassion,
      footerNewsletter,
      homeSliders,
      homeCategories,
      siteUrl
    }
  }

}

export default Home