import Head from "next/head";
import Layout from "../components/layout";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";
import { server } from '../config';
import getConfig from "next/config";

const Locator = ({ locator, locators, siteUrl }) => {

  const router = useRouter();
  const { publicRuntimeConfig } = getConfig();

  var gecode_api_url = "https://maps.googleapis.com/maps/api/js?key="+publicRuntimeConfig.GOOGLE_API_KEY+"&callback=initMap&libraries=places";

  function makeBreadcrumbJobSchema() {
      return {
           "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [{
              "@type": "ListItem",
              "position": 1,
              "name": "Home",
              "item": siteUrl
            },{
              "@type": "ListItem",
              "position": 2,
              "name": "Locator"
            }]
        }
    }

  return (

    <Layout>
      <Head>
        <title>{locator.seo ? locator.seo.title_tag : 'Locator'}</title>
        <meta name="title" content={locator.seo ? locator.seo.title_tag : ''}/>
        <meta name="description" content={locator.seo ? locator.seo.meta_description : ''}/>
        <link rel="canonical" href={siteUrl + '/locator'} />
        <script
              type='application/ld+json'
              dangerouslySetInnerHTML={{ __html: JSON.stringify(makeBreadcrumbJobSchema()) }}
          />
        <script src="js/swishersweets.js"></script>
      </Head>
	  	<div className="container mx-auto px-4">
  			<div className="products-head pt-10 pb-10 md:pb-24 md-max:-m-4 md-max:px-4 bg-green-700 text-center bg-main-pattern-t bg-bottom bg-x-full bg-no-repeat">
  				<div className="text-lg text-white uppercase">{locator.tagline}</div>
  				<h1 id="page_title" className="text-3xl py-3 font-bold uppercase text-white">{locator.title}</h1>
  				<div className="mx-auto text-white" dangerouslySetInnerHTML={{
                __html: locator.short_description
                }}></div>
  			</div>
        <div className="locator-section md:flex py-16">
          <div className="md:w-1/2 lg:w-1/3 md-max:pb-8 md:pr-8">
              <div id="stores-list-panel" className="store-page-sidebar">
                <div className="fusion-column-wrapper" data-bg-url="">
                  <div className="fusion-builder-row fusion-builder-row-inner fusion-row store-search">
                    <div className=" store-input fusion-layout-column fusion_builder_column fusion_builder_column_3_5 fusion-builder-nested-column-1 fusion-three-fifth fusion-column-first 3_5">
                      <div className="fusion-column-wrapper fusion-column-wrapper-1" data-bg-url="">
                        <input id="store-search" type="text" className="border-gray-700 border px-4 py-3 w-full bg-no-repeat pac-target-input" autoComplete="off" placeholder="Enter your zip code" />
                         <a id="locator-search-button" className="button  fusion-button button-flat fusion-button-default-size button-default button-1 fusion-button-default-span fusion-button-default-type" title="Search for nearby stores" href="#" target="_self">
                            <span className="fusion-button-text">Find</span>
                          </a>
                        <div className="mt-2" id="locator-no-results" style={{ display: "none" }}>No stores found.</div>
                      </div>
                    </div>
                  </div>
                  <div id="locator-list-pane">&nbsp;</div>
                </div>
            </div>
          </div>
          <div className="w-full" style={{ height: "600px" }} id="stores-map"></div>
        </div>
  			<div className="pb-12">
  				<div dangerouslySetInnerHTML={{
                __html: locator.description
                }} />
  			</div>
	  	</div>
      <script defer="defer" src={gecode_api_url}></script>
      </Layout>
  )
}

export async function getServerSideProps() {

  const {API_URL} = process.env
  const res = await fetch(`${API_URL}/locator`)
  const data  = await res.json()

  const res_locators = await fetch(`${API_URL}/creek-locators`)
  const locators  = await res_locators.json()

  const siteUrl = server

  return {
    props: {
      locator: data,
      locators,
      siteUrl
    }
  }

}

export default Locator