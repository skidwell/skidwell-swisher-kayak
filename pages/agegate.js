import Head from "next/head";
import Layout from "../components/layout";
import Header from "../components/header";
import Footer from "../components/footer";
import Link from "next/link";
import AgeGate from "../components/age_gate";
import React, { useEffect, useState } from "react";
import getConfig from "next/config";
import { server } from '../config';

const Agegate = ({ agegate, headerlogo, siteUrl }) => {
  
  return (
      <Layout>
        <Head>
          <title>{agegate.seo ? agegate.seo.title_tag : 'Agegate'}</title>
          <meta name="title" content={agegate.seo ? agegate.seo.title_tag : ''}/>
          <meta name="description" content={agegate.seo ? agegate.seo.meta_description : ''}/>
          <link rel="canonical" href={siteUrl + '/agegate'} />
        </Head>
        <div className="logo py-6">
          <a href="/">
              <img className="mx-auto cursor-pointer" src={headerlogo.logo.url} alt={headerlogo.logo.url} width="112" height="53" />
          </a>
       </div>
        <AgeGate agegate={agegate}/>
      </Layout>
  )
}

export async function getServerSideProps() {

  const {API_URL} = process.env
  const res = await fetch(`${API_URL}/age-gate`)
  const agegate  = await res.json()

  const res_headerlogo = await fetch(`${API_URL}/header-logo`)
  const headerlogo  = await res_headerlogo.json()

  const siteUrl = server

  return {
    props: {
      agegate,
      headerlogo,
      siteUrl
    }
  }
}


export default Agegate 
