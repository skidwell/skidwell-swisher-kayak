import Head from "next/head";
import Layout from "../components/layout";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import { server } from '../config';
import getConfig from "next/config";

const PrivacyPolicy = ({ privacyPolicy, siteUrl }) => {

	const router = useRouter();

  function makeBreadcrumbJobSchema() {
      return {
           "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [{
              "@type": "ListItem",
              "position": 1,
              "name": "Home",
              "item": siteUrl
            },{
              "@type": "ListItem",
              "position": 2,
              "name": privacyPolicy.title ? privacyPolicy.title : "Privacy Policy"
            }]
        }
    }

  return (
    <Layout>
      <Head>
        <title>{privacyPolicy.seo ? privacyPolicy.seo.title_tag : 'Privacy Policy'}</title>
        <meta name="title" content={privacyPolicy.seo ? privacyPolicy.seo.title_tag : ''} />
        <meta name="description" content={privacyPolicy.seo ? privacyPolicy.seo.meta_description : ''} />
        <link rel="canonical" href={siteUrl + '/privacy-policy'} />
        <script
            type='application/ld+json'
            dangerouslySetInnerHTML={{ __html: JSON.stringify(makeBreadcrumbJobSchema()) }}
        />
      </Head>
	  	<div className="container mx-auto px-4">
  			<div className="products-head pt-10 pb-10 md:pb-24 md-max:-m-4 md-max:px-4 bg-green-700 text-center bg-main-pattern-t bg-bottom bg-x-full bg-no-repeat">
  				<div className="text-lg text-white uppercase">{privacyPolicy.tagline}</div>
  				<h1 id="page_title" className="text-3xl py-3 font-bold uppercase text-white">{privacyPolicy.title}</h1>
  				<div className="mx-auto text-white" dangerouslySetInnerHTML={{
                __html: privacyPolicy.short_description
                }} />
  			</div>

        <div className="product-details max-w-5xl py-12 mx-auto">
          <div className="leading-8 text-gray-600" dangerouslySetInnerHTML={{
                __html: privacyPolicy.description
                }} />
        </div>

        <div className="container mx-auto px-4">
          {/* START PRIVVACY POLICY NOTICE SCRIPT  */}
          <div id="privacy-policy-container">&nbsp;</div>

          {/* START CA NOTICE SCRIPT */}
          <div id="ca-supply-chain-container"></div>
        </div>
              
            
        <script src="js/privacypolicy.js"></script>
	  	</div>
    </Layout>
  );
}

export async function getServerSideProps() {

  const {API_URL} = process.env
  const res = await fetch(`${API_URL}/privacy-policy`)
  const privacyPolicy  = await res.json()

  const siteUrl = server

  return {
    props: {
      privacyPolicy,
      siteUrl
    }
  }

}

export default PrivacyPolicy