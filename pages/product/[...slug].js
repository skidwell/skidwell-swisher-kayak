import Head from "next/head";
import Layout from "../../components/layout";
import Getme from "../../components/getme";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";
import Link from "next/link";
import ImageGallery from 'react-image-gallery';
import productReviews from "../../components/product_reviews"
import { server } from '../../config';
import getConfig from "next/config";

const Product = ({ product, reviews, footerNewsletter, siteUrl }) => {

  const router = useRouter();

  const images = []
  const imagesArr = []
  for (const image of product.image) {
    images.push({original:image.url, thumbnail:image.formats.thumbnail.url});
    imagesArr.push(image.url);
  }

  const productTab = (id) =>  {
      $(".product_tab").hide();
      $("#tab_"+id).show();
      $("#btn_description").removeClass();
      $("#btn_specification").removeClass();
      $("#btn_reviews").removeClass();
      $("#btn_"+id).addClass("font-bold underline");
  }

  var author;
  const ratings = []

  for (const review of reviews) {
    author +=  review.name + ",";
    if(review.rating == 'one'){
      var rating = 1;
    } else if(review.rating == 'two'){
      var rating = 2;
    } else if(review.rating == 'three'){
      var rating = 3;
    } else if(review.rating == 'four'){
      var rating = 4;
    } else{
      var rating = 5;
    }
    ratings.push({avg: rating});
  }

  let reviewTotal = 0;
  let length = ratings.length;
  ratings.forEach(({avg})=> reviewTotal += avg);
  if(reviewTotal > 0){
    var avgRating = reviewTotal/length;
  }else{
   var avgRating = 0;  
  }
  console.log(avgRating)

  const ratingHtml = '<ul class="rating" data-rating="'+avgRating+'"><li class="rating__item"></li><li class="rating__item"></li><li class="rating__item"></li><li class="rating__item"></li><li class="rating__item"></li></ul>';

  const bestRating = ratings.reduce((acc, shot) => acc = acc > shot.avg ? acc : shot.avg, 0)

  function makeJobSchema() {
    return {
         "@context": "https://schema.org/",
          "@type": "Product",
          "name": product.title,
          "sku": product.sku,
          "image": imagesArr,
          "description": product.description,
          "brand": {
            "@type": "Brand",
            "name": product.brand ? product.brand : "Kayak"
          },
          "review": {
            "@type": "Review",
            "reviewRating": {
              "@type": "Rating",
              "ratingValue": avgRating,
              "bestRating": bestRating
            },
            "author": {
              "@type": "Person",
              "name": author
            }
          },
          "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": avgRating,
            "reviewCount": reviews.length
          }
      }
  }

  function makeBreadcrumbJobSchema() {
    return {
         "@context": "https://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "Home",
            "item": siteUrl
          },{
            "@type": "ListItem",
            "position": 2,
            "name": product.category.title,
            "item": siteUrl + "/category/" + product.category.slug
          },{
            "@type": "ListItem",
            "position": 3,
            "name": product.title
          }]
      }
  }

  return (

    <Layout>
        <Head>
          <title>{product.seo ? product.seo.title_tag : product.title}</title>
          <meta name="title" content={product.seo ? product.seo.title_tag : ''}/>
          <meta name="description" content={product.seo ? product.seo.meta_description : ''}/>
          <link rel="canonical" href={siteUrl + '/product/' + product.slug} />
          <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
          <script
              type='application/ld+json'
              dangerouslySetInnerHTML={{ __html: JSON.stringify(makeJobSchema()) }}
          />
          <script
              type='application/ld+json'
              dangerouslySetInnerHTML={{ __html: JSON.stringify(makeBreadcrumbJobSchema()) }}
          />
        </Head>
	  	    <div className="container mx-auto px-4">
              <div className="breadcrumb my-4 uppercase text-gray-400">
                <Link href="/products"><a className="pr-4 hover:text-gray-700">Products</a></Link> / 
                <Link href={ '/category/' + product.category.slug }><a className="px-4 hover:text-gray-700">{product.category.title}</a></Link> / 
                <strong className="font-bold  px-4 text-gray-700">{product.title}</strong>
              </div>
                <div className="product-section my-8 md:flex">
                  <div className="product-gallery md:w-1/2 md:pr-8">
                    <ImageGallery
                      infinite={true}
                      showThumbnails={true}
                      thumbnailPosition={'left'}
                      showFullscreenButton={true}
                      showPlayButton={false}
                      showNav={false}
                      items={images}
                    />
                  </div>
                  <div className="product-details md:w-1/2 md:pl-8">
                    <div className="product-title text-xl text-gray-700 uppercase font-bold mb-4"><h1>{product.title}</h1></div>
                    <div className="flex items-center my-3">
                      <div className="price font-semibold text-lg my-4">${product.price}</div>
                      <div className="flex items-center mx-3" dangerouslySetInnerHTML={{
                      __html: ratingHtml
                      }} />
                      {  avgRating > 0 ? avgRating : ''}
                    </div>
                    <div className="text-gray-700 my-4" dangerouslySetInnerHTML={{
                      __html: product.short_description
                      }}></div>
                  </div>
                </div>

            <div className="tabing-products my-8">
              <div className="product-description max-w-6xl mx-auto">
              <div className="product_tab pt-10" id="tab_reviews">
                {productReviews(reviews,product.id,product.title)}
              </div>
          </div>
        </div>

              <div className="related-products mb-8 mt-20">
              <div className="product_items max-w-6xl mx-auto" id="category_long-cut">
                <div className="block-title text-3xl font-bold uppercase pb-4 text-gray-700">Related Products</div>
                <div className="product-list flex flex-wrap md-max:flex-col md:-mx-8">
                  {product.related_products.map((relproduct, i) => (
                    <Link href={`/product/${relproduct.slug}`} key={i}>
                      <div className="product-item md:text-center cursor-pointer md:w-1/2 lg:w-1/3 p-8 md-max:px-0 md-max:py-2 md-max:flex">
                        <div className="product-image bg-gray-200 px-6 py-10 md-max:p-2 md-max:w-28">
                        <img className="mx-auto" src={relproduct.image[0].url} alt={relproduct.image[0].url} /></div>
                        <div className="product-content p-6 border-3 border-solid border-gray-200 md-max:flex-grow">
                          <div className={ 'text-' + relproduct.color + '-700 font-bold uppercase text-2xl md-max:text-lg'}>{relproduct.title}</div>
                          <div className="price font-semibold text-lg">${relproduct.price}</div>
                        </div>
                      </div>
                    </Link>
                  ))}
                </div>
              </div>
            </div>
          </div>
          <Getme footerNewsletter={footerNewsletter} />
      </Layout>
  )
}

export async function getServerSideProps(context) {

  const {API_URL} = process.env

  const slugArr = context.query.slug;
  const slug = slugArr[0]

  const res_product = await fetch(`${API_URL}/products?slug=${slug}`)
  const product  = await res_product.json()
  const productId = product[0].id
  //reviews
  const resReview = await fetch(`${API_URL}/product-reviews?product=${productId}`)
  const reviewData = await resReview.json()

  const res_footernewsletter = await fetch(`${API_URL}/footer-newsletter`)
  const footerNewsletter  = await res_footernewsletter.json()

  const siteUrl = server

  return {
    props: {
      product: product[0],
      reviews: reviewData,
      footerNewsletter,
      siteUrl
    }
  }

}

export default Product