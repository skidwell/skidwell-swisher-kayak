import Head from "next/head";
import Layout from "../components/layout";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import Link from "next/link";
import React, { useRef } from "react";
import { useForm } from 'react-hook-form';
import getConfig from "next/config";
import { server } from '../config';

const ChangePassword = ({ code, forgotData, headerlogo, siteUrl }) => {

    const router = useRouter();
    const { register, errors, handleSubmit, watch } = useForm({});
    const password = useRef({});
    password.current = watch("password", "");

    async function onSubmit(formData) {

        const userInfo = {
            code: code,
            password: formData.password,
            passwordConfirmation: formData.confirm_password
        }

        const reset = await fetch(`/api/reset-password`, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userInfo)
        })
        const response = await reset.json();
        
        if(response.error){
            const message = response.message[0].messages[0].message
            $("#error_message").text(message)
            $("#error_message").show()
            $("#success_message").hide()
        }else{
            if(response.jwt){
                $("#error_message").hide()
                $("#success_message").show()
                $("input").val('')
            }else{
                $("#error_message").text("Somthing went wrong. Please try again.")
                $("#error_message").show()
                $("#success_message").hide()
            }
        }

    }

    function floatingLabel(e){
      var input = e.target
      $(input).parents(".floating_input").addClass("floated")
    }

    function floatingLabelOut(e){
      var input = e.target
      if(! $(input).val() ){
          $(input).parents(".floating_input").removeClass("floated")
      }
    }

	return (
        <Layout>
            <Head>
              <title>{forgotData.seo ? forgotData.seo.title_tag : 'Change Password'}</title>
              <meta name="title" content={forgotData.seo ? forgotData.seo.title_tag : ''}/>
              <meta name="description" content={forgotData.seo ? forgotData.seo.meta_description : ''}/>
              <link rel="canonical" href={siteUrl + '/change-password'} />
            </Head>
			<div className="login_register md:flex md:h-screen">
				<div style={{ backgroundImage: "url(" + forgotData.image.url + ")" }} className="lr_left_image md-max:hidden md:bg-cover lg:w-1/2 md-tab:w-1/2 bg-login-img bg-no-repeat bg-center"></div>
				<div className="lr_right_section md:self-center md:justify-self-center md:flex-grow text-center">
					<div className="lg:w-4/6 lg:mx-auto p-8">
						<div className="logo mb-6 cursor-pointer">
                            <a href="/">
                                <img className="mx-auto" src={headerlogo.logo.url} alt={headerlogo.logo.url} width="112" height="53" />
                            </a>
                        </div>
						<h2 className="font-black my-8 text-5xl md-max:text-3xl uppercase text-gray-700">{forgotData.title}</h2>
						<div className="my-8" dangerouslySetInnerHTML={{
                            __html: forgotData.description
                            }} />
						<div className="login_tab">
                            <div id="error_message" className="py-3 px-4 text-left bg-red-100 border-2 border-red-300 border-dashed" style={{display: "none"}}></div>
                            <div className="bg-green-200 py-3 px-4 border-2 border-green-300 border-dashed" id="success_message" style={{display: "none"}}>Your password was changed successfully. Go to <Link href="/login"><a className="underline">login</a></Link> page. </div>
							<form onSubmit={handleSubmit(onSubmit)}>
								<div className="my-6 floating_input">
                                    <label>Password *</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="password" name="password" 
                                        ref={register({
                                          required: "Please enter password",
                                          minLength: {
                                            value: 5,
                                            message: "Password must have at least 5 characters"
                                          }
                                        })}
                                    />
                                    {errors.password && <div className="text-left text-red-500">{errors.password.message}</div>}
                                </div>
								<div className="my-6 floating_input">
                                    <label>Confirm Password *</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="password" name="confirm_password" 
                                        ref={register({
                                            required: "Please enter confirm password",
                                            validate: value =>
                                            value === password.current || "Password & Confirm Password not match"
                                        })}

                                    />
                                    {errors.confirm_password && <div className="text-left text-red-500">{errors.confirm_password.message}</div>}
                                </div>
								<div className="my-6"><button className="w-full uppercase px-2 py-4 font-bold text-white bg-green-700 hover:bg-black focus:outline-none" type="submit">submit</button></div>
							</form>
						</div>
					</div>
				</div>
			</div>
        </Layout>
	);
}

export async function getServerSideProps(context) {

    const query = context.query.code;

    const {API_URL} = process.env

    const res_forgot = await fetch(`${API_URL}/change-password-page`)
    const forgotData  = await res_forgot.json()

    const res_headerlogo = await fetch(`${API_URL}/header-logo`)
    const headerlogo  = await res_headerlogo.json()

    const siteUrl = server

  return {
    props: {
        code: query,
        forgotData,
        headerlogo,
        siteUrl
    }
  }
}

export default ChangePassword;