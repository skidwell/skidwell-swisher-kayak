import Head from "next/head";
import Layout from "../../components/layout";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";
import {hubList} from "../../components/hub_list";
import Link from "next/link";
import { server } from '../../config';

const HubCategory = ({ hubs, slug, hubCategories, page, numberOfHubs, currentHub, hubBanner, siteUrl }) => {

  const router = useRouter();

  function makeBreadcrumbJobSchema() {
    return {
         "@context": "https://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "Home",
            "item": siteUrl
          },{
            "@type": "ListItem",
            "position": 2,
            "name": "Hub",
            "item": siteUrl + "/hub"
          },{
            "@type": "ListItem",
            "position": 3,
            "name": currentHub.title
          }]
      }
  }

  const lastPage = Math.ceil(numberOfHubs / 3)

  let prevbtn = []
  let nextbtn = []
  let rows = [];
  
  if(numberOfHubs > 3){
    if(page <= 1){
      prevbtn.push(<button key="90" onClick={() => router.push(`/hub?page=${page - 1}`)} disabled="disabled" className="py-2 font-bold text-orange-700 hover:text-gray-700 focus:outline-none cursor-default opacity-50 hover:text-orange-700">&lsaquo; PREV</button>)
    }else{
      prevbtn.push(<button key="100" onClick={() => router.push(`/hub?page=${page - 1}`)} className="py-2 font-bold text-orange-700 hover:text-gray-700 focus:outline-none">&lsaquo; PREV</button>)
    }

    if(page >= lastPage){
      nextbtn.push(<button key="101" className="py-2 font-bold text-orange-700 hover:text-gray-700 focus:outline-none cursor-default opacity-50 hover:text-orange-700" onClick={() => router.push(`/hub?page=${page + 1}`)} disabled="disabled">NEXT &rsaquo;</button>)
    }else{
      nextbtn.push(<button key="102" className="py-2 font-bold text-orange-700 hover:text-gray-700 focus:outline-none" onClick={() => router.push(`/hub?page=${page + 1}`)}>NEXT &rsaquo;</button>)
    }

    for (let i = 1; i < lastPage+1; i++) {
      if(i == page){
        rows.push(
            <button key={i} className="px-4 py-2 font-bold underline text-orange-700 hover:text-gray-700 focus:outline-none cursor-default" onClick={() => router.push(`/hub?page=${i}`)} disabled="disabled">{i}</button>
          );
      }else{
          rows.push(
            <button key={i} className="px-4 py-2 text-gray-700 hover:bg-orange-700 focus:outline-none" onClick={() => router.push(`/hub?page=${i}`)}>{i}</button>
          );
      }
    }
  }

  return (
    <Layout>
      <Head>
        <title>{currentHub.seo ? currentHub.seo.title_tag : 'Hub'}</title>
        <meta name="title" content={currentHub.seo ? currentHub.seo.title_tag : ''}/>
        <meta name="description" content={currentHub.seo ? currentHub.seo.meta_description : ''}/>
        <link rel="canonical" href={siteUrl + '/hub/' + currentHub.slug} />
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v1.8.0/dist/alpine.js" defer></script>
        <script
              type='application/ld+json'
              dangerouslySetInnerHTML={{ __html: JSON.stringify(makeBreadcrumbJobSchema()) }}
          />
      </Head>
    <div className="container mx-auto px-4">
      <div className="products-head py-10 md:py-14 md-max:-m-4 md-max:px-4 bg-gray-700 text-center">
        <h1 id="page_title" className="text-3xl pb-4 font-bold uppercase text-white">{hubBanner.title}</h1>
          <div className="product_filters text-center">
            <ul className="text-center">
              <li className="text-white inline-block px-4 font-light">
                <Link href="/hub">
                <a id="filter_all" data-id="all">View All</a>
                </Link>
              </li>
              {hubCategories.map((category, i) => (
              <li key={i} className="border-l-2 border-solid border-white text-white inline-block px-4 font-light">
                <Link href={'/hub/'+category.slug}><a id={'filter_'+category.slug} className={slug === category.slug ? 'text-orange-700 font-bold underline' : ''} >{category.title}</a></Link>
              </li>
            ))}
            </ul>
          </div>
      </div>

      {numberOfHubs == 0 &&
      <div className="hub-empty py-32 max-w-4xl mx-auto text-center">
        <h3 class="font-medium text-gray-700 text-lg uppercase md-max:text-base">Don't Worry, There's More</h3>
        <h2 className="font-medium text-gray-700 font-black text-7xl md-tab:text-5xl md-max:text-5xl py-3 uppercase">Coming Soon</h2>
      </div>
      }

      {numberOfHubs > 0 &&
      <div className="max-w-6xl py-14 mx-auto">
        <div className="hub-section">
        {hubs.map((hub, i) => (
          <div className="hub-item" key={i}>
          { hubList(hub) }
          </div>
        ))}
        </div>
        <div className="pagination my-4">
          <nav className="flex justify-between" aria-label="Pagination">
            <div>
              {prevbtn}
            </div>
            <div className="md-max:hidden">
              {rows}
            </div>
            <div>
              {nextbtn}
            </div>
          </nav>
        </div>
      </div>
      }
    </div>
    </Layout>
  );
}


export async function getServerSideProps(context) {

  const slugArr = context.query.slug;
  const slug = slugArr[0]

  const {API_URL} = process.env
  var page = 1
  if (context.query.page){
    page = context.query.page
  }

  const start = +page === 1 ? 0 : (+page - 1) * 3

  const hubCatRes = await fetch(`${API_URL}/hub-categories?slug=${slug}`)
  const hubCat = await hubCatRes.json()

  const id = hubCat[0].id

  const numberOfHubsResponse = await fetch(`${API_URL}/hubs/count?hub_category=${id}`)
  const numberOfHubs = await numberOfHubsResponse.json()
  
  const res = await fetch(`${API_URL}/hubs?_limit=3&_start=${start}&hub_category=${id}`)
  const data  = await res.json()

  const res_cat = await fetch(`${API_URL}/hub-categories`)
  const hubCategories  = await res_cat.json()

  const res_hubpagebanner = await fetch(`${API_URL}/hub-page-banner`)
  const hubBanner  = await res_hubpagebanner.json()

  const siteUrl = server

  return {
    props: {
      hubs: data,
      slug: slug,
      hubCategories: hubCategories,
      page: +page,
      numberOfHubs,
      currentHub: hubCat[0],
      hubBanner,
      siteUrl
    }
  }

}


export default HubCategory