import Head from "next/head";
import Layout from "../components/layout";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import nookies from 'nookies'
import { parseCookies  } from 'nookies'

function redirectUser(ctx, location) {
    if (ctx.req) {
        ctx.res.writeHead(302, { Location: location });
        ctx.res.end();
    } else {
        Router.push(location);
    }
}

function Logout(){

  return (
    <Layout>
			<div>Redirecting to home page...</div>
    </Layout>
  );
}

Logout.getInitialProps = async (ctx) => {
	nookies.destroy(ctx, 'jwt')
  nookies.destroy(ctx, 'userInfo')
  
  let pageProps = {}
  const jwt = parseCookies(ctx).jwt

    if (!jwt) {
      redirectUser(ctx, "/");
    }else{
      redirectUser(ctx, "/account");
    }
}

export default Logout