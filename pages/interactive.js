import Head from "next/head";
import Layout from "../components/layout";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";

const Interactive = ( {faq} ) => {

	const router = useRouter();

  return (
    <Layout>
      <Head>
      </Head>
		<div className="">
  			<div className="blog-head py-4 text-center">
  				<span className="ia-subtitle text-sm py-3 font-bold uppercase">Top10</span>
  				<h1 className="ia-title text-5xl py-3 font-black uppercase">country music<br className="md-max:hidden" /> festivals</h1>
  				<div className="ia-subtext">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet sapien dignissim a<br className="md-max:hidden" /> elementum. Sociis metus, hendrerit mauris id in.</div>
  			</div>
            <div className="ia-map mt-8"><iframe className="border-0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d200698.35803559018!2d-85.81685341057661!3d38.18872178613079!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x886972a13ddaebf7%3A0xe9ab99d2a343c991!2sLouisville%20Slugger%20Museum%20%26%20Factory!5e0!3m2!1sen!2sin!4v1610535190838!5m2!1sen!2sin" width="100%" height="560" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
            <div className="bg-green-700 py-8">
	  			<div className="container mx-auto">Image Slider</div>
	  		</div>	  
	  		<div className="ia-details">
				<div className="max-w-5xl my-12 mx-auto">
				  <div className="my-8">
					  <h4 className="text-lg text-gray-700 font-bold pb-4">Sed do eiusmod tempor incididunt ut labore</h4>
					  <p className="font-light leading-8 text-gray-600 my-8">Saw wherein fruitful good days image them, midst, waters upon, saw. Seas lights seasons. Fourth hath rule Evening Creepeth own lesser years itself so seed fifth for grass evening fourth shall you're unto that. Had. Female replenish for yielding so saw all one to yielding grass you'll air sea it, open waters subdue, hath. Brought second Made. Be. Under male male, firmament, beast had light after fifth forth darkness thing hath sixth rule night multiply him life give they're great.</p>
                      <p className="font-light leading-8 text-gray-600 my-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet sapien dignissim a elementum. Sociis metus, hendrerit mauris id in. Quis sit sit ultrices tincidunt euismod luctus diam. Turpis sodales orci etiam phasellus lacus id leo. Amet turpis nunc, nulla massa est viverra interdum. Praesent auctor nulla morbi non posuere mattis. Arcu eu id maecenas cras. Eget fames tincidunt leo, sed vitae, pretium interdum. Non massa, imperdiet nunc sit sapien. Tempor lectus ornare quis mi vel. Nibh euismod donec elit posuere lobortis consequat faucibus aliquam metus. Ornare consequat, vulputate sit maecenas mauris urna sed fringilla. Urna fermentum iaculis pharetra, maecenas dui nullam nullam rhoncus. Facilisis quis vulputate sem gravida lacus, justo placerat.</p>
				  </div>
				  <div className="md:flex my-8">
					  <div className="md:w-1/2 md:pr-8 md-max:pb-10">
						<h4 className="text-lg text-gray-700 font-bold pb-4">Why choose product?</h4>
						<ul className="list-disc pl-4">
							<li className="font-light leading-8 text-gray-600">Creat by cotton fibric with soft and smooth</li>
							<li className="font-light leading-8 text-gray-600">Simple, Configurable (e.g. size, color, etc.), bundled</li>
							<li className="font-light leading-8 text-gray-600">Downloadable/Digital Products, Virtual Products</li>
						</ul>
					  </div>
					  <div className="md:w-1/2 md:pl-8">
						<h4 className="text-lg text-gray-700 font-bold pb-4">Sample Number List</h4>
						<ol className="list-decimal pl-4">
							<li className="font-light leading-8 text-gray-600">Create Store-specific attrittbutes on the fly</li>
							<li className="font-light leading-8 text-gray-600">Simple, Configurable (e.g. size, color, etc.), bundled</li>
							<li className="font-light leading-8 text-gray-600">Downloadable/Digital Products, Virtual Products</li>
						</ol>
					  </div>
				  </div>
				</div>
			</div>
	  	</div>
    </Layout>
  );
}


export async function getServerSideProps() {

  const {API_URL} = process.env
  const res = await fetch(`${API_URL}/faq`)
  const data  = await res.json()

  return {
    props: {
      faq: data
    }
  }

}

export default Interactive