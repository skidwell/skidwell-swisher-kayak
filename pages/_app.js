import "../styles/globals.scss";
import Router , {useRouter}  from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import Footer from "../components/footer";
import Header from "../components/header";
import Warning from "../components/warning";
import AgeGate from "../components/age_gate";
import getConfig from 'next/config'
import { parseCookies  } from 'nookies'
import { setCookie } from 'nookies'
import { server } from '../config';
import { useCookies } from "react-cookie";
import { useEffect } from 'react';
import TagManager from 'react-gtm-module';
 
const tagManagerArgs = {
    gtmId: 'GTM-MZBN7Q'
}

// TagManager.initialize(tagManagerArgs);

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => {
    NProgress.done();
    if($('#mobile_menu').css('display') != 'none') {
        $("#mobile_menu").hide();
    };
});
Router.events.on('routeChangeError', () => NProgress.done());

function IEdetection() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older, return version number
        return ('IE ' + parseInt(ua.substring(
        msie + 5, ua.indexOf('.', msie)), 10));
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11, return version number
        var rv = ua.indexOf('rv:');
        return ('IE ' + parseInt(ua.substring(
        rv + 3, ua.indexOf('.', rv)), 10));
    }
    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        //Edge (IE 12+), return version number
        return ('IE ' + parseInt(ua.substring(
        edge + 5, ua.indexOf('.', edge)), 10));
    }
    // User uses other browser
    return false;
}

function MyApp({ Component, pageProps, footerData, footerLinks, navigations, warning, headerlogo, agegate }) {

    useEffect(() => {
        TagManager.initialize(tagManagerArgs)
    }, [])

    if (typeof window !== 'undefined') {
        window.scrollTo(0, 0);
    }

    const [cookies, setCookie] = useCookies(["agegate", "agegate_session"]);
    var age_gate = cookies.agegate;
    var isSessionExpired = cookies.agegate_session;

    if (typeof window !== 'undefined') {

        var isIE = IEdetection();
        console.log(isIE)

        if(isIE){
            var warningPage = document.getElementById("warning-section");
                (warningPage) ? warningPage.style.display = "none" : null;

            var mainPage = document.getElementById("main_page_content");
                (mainPage) ? mainPage.style.display = "none" : null;

            var agegatePage = document.getElementById("age_gate_page");
                (agegatePage) ? agegatePage.style.display = "none" : null;

            var browserPage = document.getElementById("browser_not_support");
                (browserPage) ? browserPage.style.display = "block" : null;

        }else if(!age_gate || age_gate<21){
            var mainPage = document.getElementById("main_page_content");
            (mainPage) ? mainPage.style.display = "none" : null;

            var agegatePage = document.getElementById("age_gate_page");
            (agegatePage) ? agegatePage.style.display = "block" : null;
        } else if(!isSessionExpired){
            console.log("agegate cookie expired");
            setCookie("agegate", "", {
                path: "/",
                maxAge: -1,
                sameSite: true,
                secure: true
              });
            var mainPage = document.getElementById("main_page_content");
                (mainPage) ? mainPage.style.display = "none" : null;

            var agegatePage = document.getElementById("age_gate_page");
                (agegatePage) ? agegatePage.style.display = "block" : null;
        } else{
            var mainPage = document.getElementById("main_page_content");
                (mainPage) ? mainPage.style.display = "block" : null;

            var agegatePage = document.getElementById("age_gate_page");
            (agegatePage) ? agegatePage.style.display = "none" : null;
        }
    }

  return (
  	<>
        <Warning warning={warning}/>
        <div className="main-content" id="main_page_content">
            <Header navigations = {navigations} warning={warning} headerlogo={headerlogo} />
            <Component {...pageProps} />
            <Footer footerData={footerData} headerlogo={headerlogo} footerLinks = {footerLinks} />
        </div>
        <div className="agegate-page" id="age_gate_page" style={{display: "none" }}>
            <div className="logo py-6">
              <a href="/">
                  <img className="mx-auto cursor-pointer" src={headerlogo.logo.url} alt={headerlogo.logo.url} width="112" height="53" />
              </a>
            </div>
            <AgeGate agegate={agegate}/>
        </div>
        <div className="browser-not-supported " id="browser_not_support" style={{display: "none" }}>
            <h3 className="text-lg font-medium font-bold uppercase leading-normal text-3xl">
                The browser is no longer supported. Please use a different browser to view this page.
            </h3>
        </div>
    </>
  )
}

const { publicRuntimeConfig } = getConfig()

function redirectUser(ctx, location) {
    if (ctx.req) {
        ctx.res.writeHead(302, { Location: location });
        ctx.res.end();
    } else {
        Router.push(location);
    }
}

MyApp.getInitialProps = async ({Component, ctx}) => {

	let pageProps = {}
    const jwt = parseCookies(ctx).jwt

    if (Component.getInitialProps) {
        pageProps = await Component.getInitialProps(ctx)
    }

    if (!jwt) {
        if (ctx.pathname === "/account") {
            redirectUser(ctx, "/login");
        }
    }

    const res_footer = await fetch(`${publicRuntimeConfig.API_URL}/footer`)
    const footerData  = await res_footer.json()

    const res_footerlinks = await fetch(`${publicRuntimeConfig.API_URL}/footer-links`)
    const footerLinks  = await res_footerlinks.json()

    const res_navigations = await fetch(`${publicRuntimeConfig.API_URL}/navigations?_sort=id:asc`)
    const navigations  = await res_navigations.json()

    const res_warning = await fetch(`${publicRuntimeConfig.API_URL}/header-warning`)
    const warning  = await res_warning.json()

    const res_headerlogo = await fetch(`${publicRuntimeConfig.API_URL}/header-logo`)
    const headerlogo  = await res_headerlogo.json()

    const res = await fetch(`${publicRuntimeConfig.API_URL}/age-gate`)
    const agegate  = await res.json()

    return { pageProps, footerData, footerLinks, navigations, warning, headerlogo, agegate}
}

export default MyApp
