import Head from "next/head";
import Layout from "../components/layout";
import React, { useRef, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";
import { parseCookies  } from 'nookies'
import { setCookie } from 'nookies'
import { useForm } from 'react-hook-form';
import { server } from '../config';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

const Account = ({countries, accountContent, siteUrl}, ctx) => {

	const router = useRouter();

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    async function handleStates(countryName) {

        if(countryName){
            var countryStates = countries.filter(function (entry) {
                return entry.name === countryName;
            });

            if(countryStates[0].states){
                var stateList = '<select id="state_name" class="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent"><option value="">State</option>';
                for (const [i, state] of Object.entries(countryStates[0].states)) {
                    if(state.name == user.state){
                        stateList +=  '<option value="'+state.name+'" selected="selected">'+state.name+'</option>'
                    }else{
                        stateList +=  '<option value="'+state.name+'">'+state.name+'</option>'
                    }
                }
                stateList +=  '</select>'

                $("#state_list").html(stateList)
            }else{
                $("#state_list").html('<input id="state_name" class="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" placeholder="State"/>');
            }
        }else{
            $("#state_list").html('<input id="state_name" class="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" placeholder="State"/>');
        }
    }


    const jwt = parseCookies(ctx).jwt

    var user = []
    var user_info = parseCookies(ctx).userInfo;
    if(user_info){
        var user = JSON.parse(user_info)
        console.log(user)
    }

    var dateFormat = require("dateformat");

    const [first_name, setFirstName] = useState(user.first_name)
    const [last_name, setLastName] = useState(user.last_name)
    const [display_name, setDisplayName] = useState(user.display_name)
    const [phone_number, setPhoneNumber] = useState(user.phone_number)
    const [email, setEmail] = useState(user.email)
    const [address_line_1, setAddressLiine1] = useState(user.address_line_1)
    const [address_line_2, setAddressLiine2] = useState(user.address_line_2)
    const [city, setCity] = useState(user.city)
    const [state, setState] = useState(user.state)
    const [zipcode, setZipcode] = useState(user.zipcode)

    var currentDOb = user.birthday ? new Date(user.birthday) : '';
    const [startDate, setStartDate] = useState(currentDOb);

    async function handleSave() {
        const userInfo = {
            id: user.id,
            jwt: jwt,
            email: email,
            first_name: first_name,
            last_name: last_name,
            display_name: display_name,
            birthday: dateFormat($("#birthday").val(), "isoDate"),
            phone_number: phone_number,
            address_line_1: address_line_1,
            address_line_2: address_line_2,
            city: city,
            state: $("#state_name").val(),
            country: $("#country_name").val(),
            zipcode: zipcode
        }

        var email_error = false;
        if(!email){
            $("#email_error").text('Please enter your email address');
            $("#email_error").show();
            var email_error = true;
        }else{
            if(!validateEmail(email)) {
                $("#email_error").text('Please enter correct email address');
                $("#email_error").show();
                var email_error = true;
            }else{
                $("#email_error").hide();
                var email_error = false;
            }
        }

        if(email_error){
            return
        }else{

            const update = await fetch(`/api/customer-update`, {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(userInfo)
            })

            const updateResponse = await update.json()
            if(updateResponse.id){
                console.log(updateResponse)
                setCookie(null, 'userInfo', JSON.stringify(updateResponse) , {
                    maxAge: 1 * 24 * 60 * 60, // Expires after 24hrs
                    path: '/',
                })
                $("#error_message").hide()
                $("#success_message").show()
            }else{
                $("#error_message").show()
                $("#success_message").hide()
            }
        }
    }


    const { register, errors, handleSubmit, watch } = useForm({});
    const password = useRef({});
    password.current = watch("password", "");

    async function onSubmit(formData) {
        const passInfo = {
            identifier: user.username,
            password: formData.old_password,
            newPassword: formData.password,
            confirmPassword: formData.confirm_password,
            jwt: jwt
        }

        const reset = await fetch(`/api/change-password`, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(passInfo)
        })
        const response = await reset.json();
        if(response.error){
            const message = response.message[0].messages[0].message
            $("#password_error_message").text(message)
            $("#password_error_message").show()
            $("#password_success_message").hide()
        }else{
            if(response.jwt){
                $("#password_error_message").hide()
                $("#password_success_message").show()
            }else{
                $("#password_error_message").text("Somthing went wrong. Please try again.")
                $("#password_error_message").show()
                $("#password_success_message").hide()
            }
        }
    }

    function predicateBy(prop){
       return function(a,b){
          if (a[prop] > b[prop]){
              return 1;
          } else if(a[prop] < b[prop]){
              return -1;
          }
          return 0;
       }
    }

    const countriesList = []
    countries.sort( predicateBy("name") )
    if(countries.length>0){
        for (const [i, country] of Object.entries(countries)) {
            if(user.country == country.name){
                setTimeout(function(){
                    handleStates(country.name)
                }, 800)
            }
            countriesList.push(
                <option key={i} value={country.name}>{country.name}</option>
            )
        }
    }

    function floatingLabel(e){
      var input = e.target
      $(input).parents(".floating_input").addClass("floated")
    }

    function floatingLabelOut(e){
      var input = e.target
      if(! $(input).val() ){
          $(input).parents(".floating_input").removeClass("floated")
      }
    }

  return (
    <Layout>
        <Head>
            <title>{accountContent.seo ? accountContent.seo.title_tag : 'My Account'}</title>
            <meta name="title" content={accountContent.seo ? accountContent.seo.title_tag : ''}/>
            <meta name="description" content={accountContent.seo ? accountContent.seo.meta_description : ''}/>
            <link rel="canonical" href={siteUrl + '/account'} />
        </Head>
		<div className="my_account_page px-4 container mx-auto">
  			<h2 className="font-black text-6xl md-max:text-4xl text-center my-12 md-max:my-5 uppercase">Account Details</h2>
  			<div className="md:flex">
  				<div className="md:w-1/5">
  					<ul className="md-max:text-center">
  						<li className="font-bold uppercase my-6 md-max:hidden">Account Details</li>
  						<li className="my-6"><a className="font-bold uppercase hover:text-green-700" href="/logout">Logout</a></li>
  					</ul>
  				</div>
  				<div className="md:flex-grow">
                    <div className="success-message bg-green-200 py-3 px-4 border-2 border-green-300 border-dashed" id="success_message" style={{display: "none"}}>Your account was updated successfully!</div>
  					<div id="error_message" className="py-3 px-4 text-left bg-red-100 border-2 border-red-300 border-dashed" style={{display: "none"}}>Somthing went wrong. Please try again.</div>
                        <form className="form_login">
                            <div className="my-6 md:flex">
                                <div className={first_name ? 'md:w-1/2 md:pr-2 md-max:my-6 floating_input floated' : 'md:w-1/2 md:pr-2 md-max:my-6 floating_input' }>
                                    <label>First Name</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onChange={e => setFirstName(e.target.value) } value={first_name} />
                                </div>
                                <div className={last_name ? 'md:w-1/2 md:pl-2 floating_input floated' : 'md:w-1/2 md:pl-2 floating_input'}>
                                    <label>Last Name</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onChange={e => setLastName(e.target.value) } value={last_name}/>
                                </div>
                            </div>
                            <div className="my-6 md:flex">
                                <div className={display_name ? 'md:w-1/2 md:pr-2 md-max:my-6 floating_input floated' : 'md:w-1/2 md:pr-2 md-max:my-6 floating_input'}>
                                    <label>Display Name</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onChange={e => setDisplayName(e.target.value) } value={display_name} />
                                </div>
                                <div className={phone_number ? 'md:w-1/2 md:pl-2 floating_input floated' : 'md:w-1/2 md:pl-2 floating_input'}>
                                    <label>Phone Number</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="number" onChange={e => setPhoneNumber(e.target.value) } value={phone_number} />
                                </div>
                            </div>
                            <div className={email ? 'my-6 floating_input floated' : 'my-6 floating_input'}>
                                <label>Email Address *</label>
                                <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="email" onChange={e => setEmail(e.target.value) } value={email} />
                                <div id="email_error" className="text-left text-red-500" style={{display: "none"}}>Please enter email address</div>
                            </div>
                            <div className={startDate ? 'my-6 floating_input floated' : 'my-6 floating_input'}>
                                <label>Birthday</label>
                                <DatePicker
                                  name="birthday"
                                  ref={register}
                                  id="birthday"
                                  className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent"
                                  selected={startDate}
                                  onChange={date => setStartDate(date)}
                                  onFocus={floatingLabel} 
                                  onBlur={floatingLabelOut}
                                  peekNextMonth
                                  showMonthDropdown
                                  showYearDropdown
                                  dropdownMode="select"
                                  maxDate={new Date()}
                                />
                            </div>
                            <div className={address_line_1 ? 'my-6 md:flex floating_input floated' : 'my-6 md:flex floating_input'}>
                                <div className="md:w-1/2 md:pr-2 md-max:my-6">
                                    <label>Street Address</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onChange={e => setAddressLiine1(e.target.value) } value={address_line_1} />
                                </div>
                                <div className={address_line_2 ? 'md:w-1/2 md:pl-2 floating_input floated' : 'md:w-1/2 md:pl-2 floating_input'}>
                                    <label>Address Line 2</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onChange={e => setAddressLiine2(e.target.value) } value={address_line_2}/>
                                </div>
                            </div>
                            <div className="my-6 md:flex">
                                <div className={user.country ? 'md:w-1/3 md:pr-4 md-max:my-6 floating_input floated' : 'md:w-1/3 md:pr-4 md-max:my-6 floating_input'} id="country">
                                    <label>Country</label>
                                    <select id="country_name" className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" onChange={e => handleStates(e.target.value) } defaultValue={user.country}>
                                        <option value=""></option>
                                        {countriesList}
                                    </select>
                                </div>
                                <div className={zipcode ? 'md:w-1/3 md:pr-2 md-max:my-6 floating_input floated' : 'md:w-1/3 md:pr-2 md-max:my-6 floating_input'}>
                                    <label>Postal Code</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onChange={e => setZipcode(e.target.value) } value={zipcode} />
                                </div>
                                <div className={city ? 'md:w-1/3 md:pl-2 floating_input floated' : 'md:w-1/3 md:pl-2 floating_input'}>
                                    <label>City</label>
                                    <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onChange={e => setCity(e.target.value) } value={city} />
                                </div>
                            </div>
                            <div className={state ? 'my-6 floating_input floated' : 'my-6 floating_input'} id="state_list">
                                <label>State</label>
                                <input onFocus={floatingLabel} onBlur={floatingLabelOut} id="state_name" className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onChange={e => setState(e.target.value) } value={state} />
                            </div>
                            <div className="my-6">
                                <button className="uppercase px-20 py-4 font-bold text-white bg-green-700 hover:bg-black md-max:w-full focus:outline-none" type="button" onClick={() => handleSave() }>save changes</button>
                            </div>
                        </form>
                        <div id="password_error_message" className="py-3 px-4 text-left bg-red-100 border-2 border-red-300 border-dashed" style={{display: "none"}}>Somthing went wrong. Please try again.</div>
                        <div className="success-message bg-green-200 py-3 px-4 border-2 border-green-300 border-dashed" id="password_success_message" style={{display: "none"}}>Your password was changed successfully!</div>
                        <form className="form_change_password mb-20" onSubmit={handleSubmit(onSubmit)}>
                            <h3 className="font-bold text-xl mt-12 mb-6 uppercase">PASSWORD CHANGE</h3>
                            <div className="my-6 floating_input">
                                <label>Current password (leave blank to leave unchanged) *</label>
                                <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="password" name="old_password" ref={register({
                                              required: "Please enter old password" })}
                                        />
                                    {errors.old_password && <div className="text-left text-red-500">{errors.old_password.message}</div>}
                            </div>
                            <div className="my-6 floating_input">
                                <label>New password (leave blank to leave unchanged) *</label>
                                <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="password" name="password" autoComplete="new-password" ref={register({
                                              required: "Please enter password",
                                              minLength: {
                                                value: 5,
                                                message: "Password must have at least 5 characters"
                                              }
                                            })}
                                        />
                                    {errors.password && <div className="text-left text-red-500">{errors.password.message}</div>}
                            </div>
                            <div className="my-6 floating_input">
                                <label>Confirm new password *</label>
                                <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="password" name="confirm_password" ref={register({
                                            required: "Please enter confirm password",
                                            validate: value =>
                                            value === password.current || "Password & Confirm Password not match"
                                        })}
                                />
                                {errors.confirm_password && <div className="text-left text-red-500">{errors.confirm_password.message}</div>}
                            </div>
                            <div className="my-6">
                                <button className="uppercase px-20 py-4 font-bold text-white bg-green-700 hover:bg-black md-max:w-full focus:outline-none" type="submit" >change password</button>
                            </div>
                        </form>
  				</div>
  			</div>
  		</div>
    </Layout>
  );
}

export async function getServerSideProps() {

    const res_countries = await fetch(`https://api.printful.com/countries`)
    const countries  = await res_countries.json()

    const {API_URL} = process.env
    const res_account = await fetch(`${API_URL}/account-page-content`)
    const accountContent  = await res_account.json()

    const siteUrl = server

  return {
    props: {
      countries: countries.result,
      accountContent,
      siteUrl
    }
  }

}

export default Account
