import Head from "next/head";
import Layout from "../../components/layout";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";
import { parseCookies  } from 'nookies'
import getConfig from 'next/config'
import hubReviews from "../../components/reviews"
import Link from "next/link";
import { server } from '../../config';
import { applySession } from 'next-session';

const HubDetails = ({ hub, prevPost, nextPost, reviews, views, siteUrl }, ctx) => {

  const router = useRouter();
  const jwt = parseCookies(ctx).jwt;

  const [customerName, setCustomerName] = useState('')
  const [customerEmail, setCustomerEmail] = useState('')
  const [reviewComment, setReviewComment] = useState('')

  const date = new Date(hub.created_at)
  const month = date.toLocaleString('default', { month: 'long' })
  const year = date.getFullYear()
  const postDate = month + " " + year

  const hubId = hub.id

  function makeArticleJobSchema() {
    return {
        "@context": "http://schema.org",
        "@type": "Article",
            "mainEntityOfPage": {
              "@type": "WebPage",
              "@id": siteUrl + "/hub-details/" + hub.slug
            },
            "headline": hub.title,
            "image": hub.main_image.url,
            "datePublished": hub.published_at,
            "dateModified": hub.updated_at,
            "author": {
              "@type": "Person",
              "name": hub.author_name ? hub.author_name : 'Kayak'
            },
            "description": hub.description,
            "articleBody": {
              "@type": "WebPageElement",
              "cssSelector" : ".articlebody"
            },
          "isAccessibleForFree": "False",
            "hasPart": {
              "@type": "WebPageElement",
              "isAccessibleForFree": "False",
              "cssSelector" : ".contentgate"
            }
      }
  }

  function makeBreadcrumbJobSchema() {
    return {
         "@context": "https://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "Home",
            "item": siteUrl
          },{
            "@type": "ListItem",
            "position": 2,
            "name": hub.hub_category ? hub.hub_category.title : 'N/A',
            "item": siteUrl + "/hub/" + (hub.hub_category ? hub.hub_category.slug : '')
          },{
            "@type": "ListItem",
            "position": 3,
            "name": hub.title
          }]
      }
  }

  return (
    <Layout>
      <Head>
        <title>{hub.seo ? hub.seo.title_tag : hub.title}</title>
        <meta name="title" content={hub.seo ? hub.seo.title_tag : ''}/>
        <meta name="description" content={hub.seo ? hub.seo.meta_description : ''}/>
        <link rel="canonical" href={siteUrl + '/hub-details/' + hub.slug} />
        <script
            type='application/ld+json'
            dangerouslySetInnerHTML={{ __html: JSON.stringify(makeArticleJobSchema()) }}
        />
        <script
            type='application/ld+json'
            dangerouslySetInnerHTML={{ __html: JSON.stringify(makeBreadcrumbJobSchema()) }}
        />
      </Head>
      <div className="container mx-auto px-4">
        <div className="articlebody">
          <div className="blog-head py-4 text-center">
            <h1 className="blog-title text-4xl py-3 font-black uppercase">{hub.title}</h1>
            <div className="blog-post-details font-bold">From {hub.hub_category ? hub.hub_category.title : 'N/A'} | {postDate}</div>
          </div>
          <div className="blog-image my-8">
            <img className="w-full" src={hub.main_image.url} alt={hub.main_image.url} />
          </div>

          {jwt || views <= 5 ? (
              <div className="blog-details max-w-6xl my-12 mx-auto" dangerouslySetInnerHTML={{
                __html: hub.description
              }} />
            ) : (
              <div className="contentgate" dangerouslySetInnerHTML={{
                __html: hub.contentgate
              }} />
            )
          }          
          
        </div>

        <div className="pagination max-w-5xl mx-auto border-t border-b border-gray-300 py-8 my-8">
          <nav className="flex justify-between" aria-label="Pagination">
              <div className="pr-4">
              {prevPost ? ( 
              <Link href={`/hub-details/${prevPost.slug}`}><a className="block font-bold">&lsaquo; PREVIOUS POST <span className="block font-light">{prevPost.title}</span></a></Link>
              ) : '' }
              </div>
              <div className="pl-4">
              {nextPost ? ( 
                  <Link href={`/hub-details/${nextPost.slug}`}>
                    <a className="text-right font-bold block">NEXT POST &rsaquo; <span className="block font-light">{nextPost.title}</span></a>
                  </Link>
                ) : ''
              }
              </div>
          </nav>
        </div>
        {hubReviews(reviews,hubId,hub.title)}
      </div>
    </Layout>
  );
}


export async function getServerSideProps({ req, res, query }) {

  const slugArr = query.slug;
  const slug = slugArr[0]

  const {API_URL} = process.env
  
  const resHubs = await fetch(`${API_URL}/hubs?&slug=${slug}`)
  const data  = await resHubs.json()

  const hubId = data[0].id
  
  //previus post
  const res_prev = await fetch(`${API_URL}/hubs?id_lt=${hubId}&_sort=id:DESC&_limit=1`)
  const dataPrev  = await res_prev.json()

  //next post
  const res_next = await fetch(`${API_URL}/hubs?id_gt=${hubId}&_limit=1`)
  const dataNext  = await res_next.json()

  //reviews
  const resReview = await fetch(`${API_URL}/hub-reviews?hub=${hubId}`)
  const reviewData = await resReview.json()

  await applySession(req, res);
  req.session.views = req.session.views ? req.session.views + 1 : 1;

  const siteUrl = server

  return {
    props: {
      hub: data[0],
      prevPost: dataPrev[0] ? dataPrev[0] : '',
      nextPost: dataNext[0] ? dataNext[0] : '',
      reviews: reviewData,
      views: req.session.views,
      siteUrl
    }
  }

}


export default HubDetails