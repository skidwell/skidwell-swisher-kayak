import axios from "axios"

export default async (req, res) => {

  const url = 'https://api.printful.com/countries';
  await axios
    .get(url)
    .then(({ data }) => {
      res.status(200)
      res.json(JSON.stringify(data))
    })
    .catch(({ err }) => {
      res.status(400).json({ err })
    })
}
