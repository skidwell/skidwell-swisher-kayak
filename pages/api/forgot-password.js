import axios from "axios"

export default async (reqq, ress) => {
    
    const {API_ENDPOINT} = process.env

    try{
        var http = require("https");

        var options = {
          "method": "POST",
          "hostname": `${API_ENDPOINT}`,
          "port": null,
          "path": "/auth/forgot-password",
          "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache"
          }
        };

        var req = http.request(options, function (res) {
          var chunks = [];

          res.on("data", function (chunk) {
            chunks.push(chunk);
          });

          res.on("end", function () {
            var body = Buffer.concat(chunks);
            ress.status(200)
            ress.json(body.toString())
          });
        });
        var data = JSON.stringify(reqq.body);
        req.write(data);
        req.end();
    } catch (error) {
        console.log(error);
        res.status(400)
        res.json({
            "status": "error",
            "message": error
        })
    }
}