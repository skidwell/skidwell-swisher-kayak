import Error from 'next/error'
import Head from "next/head";
import Layout from "../components/layout";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import Link from "next/link";

export async function getServerSideProps() {
  
  const {API_URL} = process.env
  const res_pagenotfound = await fetch(`${API_URL}/page-not-found`)
  const pageNotFound  = await res_pagenotfound.json()

  return {
    props: { pageNotFound }
  }
}

export default function Page({ pageNotFound }) {

  return (
    <Layout>
      <Head>
        <title>{pageNotFound.seo ? pageNotFound.seo.title_tag : '404'}</title>
        <meta name="title" content={pageNotFound.seo ? pageNotFound.seo.title_tag : ''}/>
        <meta name="description" content={pageNotFound.seo ? pageNotFound.seo.meta_description : ''}/>
      </Head>
      <div className="container mx-auto text-center px-4">
        <h2 className="text-gray-700 uppercase text-5xl font-black pt-12 pb-16">{pageNotFound.tagline}</h2>
        <div className="notfound_image"><img className="mx-auto" src="/notfound.png" alt="/notfound.png" /></div>
        <h3 className="text-gray-700 uppercase text-2xl font-bold pt-10">{pageNotFound.title}</h3>
        <div className="py-10 max-w-lg mx-auto" dangerouslySetInnerHTML={{
                __html: pageNotFound.description
                }} />
        <Link href={`/`}>
          <button className="bg-green-700 hover:bg-black font-bold text-white uppercase w-80 mb-10 py-4">GO BACK</button>
        </Link>
      </div>
      </Layout>
  )
}