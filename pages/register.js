import Head from "next/head";
import Layout from "../components/layout";
import getConfig from 'next/config'
import { setCookie } from 'nookies'
import Router , {useRouter}  from 'next/router';
import Link from "next/link";
import { useCookies } from "react-cookie";
import React, { useRef, useState } from "react";
import DatePicker from "react-datepicker";
import { useForm } from 'react-hook-form';
import { server } from '../config';

import "react-datepicker/dist/react-datepicker.css";

const Register = ({ countries, registerData, footerData, headerlogo, siteUrl }) => {

    const router = useRouter();
    const [startDate, setStartDate] = useState('');
    const { register, errors, handleSubmit, watch } = useForm({});
    const password = useRef({});
    password.current = watch("password", "");

    var dateFormat = require("dateformat");

    async function onSubmit(formData) {

        const registerInfo = {
            username: formData.email,
            email: formData.email,
            password: formData.password,
            first_name: formData.first_name,
            last_name: formData.last_name,
            display_name: formData.display_name,
            birthday: dateFormat($("#birthday").val(), "isoDate"),
            phone_number: formData.phone_number,
            address_line_1: formData.address_line_1,
            address_line_2: formData.address_line_2,
            city: formData.city,
            state: $("#state_name").val(),
            country: formData.country,
            zipcode: formData.zipcode
        }

        const register = await fetch(`/api/register`, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(registerInfo)
        })
        const registerResponse = await register.json();

        if(registerResponse.error){
            const message = registerResponse.message[0].messages[0].message
            $(".error_message").text(message)
            $(".error_message").show()
            $("#success_message").hide()
        }else{
            if(registerResponse.jwt){
                $(".error_message").hide()
                setCookie(null, 'jwt', registerResponse.jwt , {
                    maxAge: 1 * 24 * 60 * 60, // Expires after 24hrs
                    path: '/',
                })
                setCookie(null, 'userInfo', JSON.stringify(registerResponse.user) , {
                    maxAge: 1 * 24 * 60 * 60, // Expires after 24hrs
                    path: '/',
                })
                Router.push('/account')
            }else if(registerResponse.user){
                $("#success_message").show()
                $(".error_message").hide()
                $('input').val('')
                $('select').val('')
            }else{
                $("#success_message").hide()
                $(".error_message").text('Somthing went wrong.')
                $(".error_message").show()
            }
        }

    }
    
    async function handleStates(countryName) {
    	
    	if(countryName){
            $("#country").addClass("floated");
	    	var countryStates = countries.filter(function (entry) {
			    return entry.name === countryName;
			});

	    	if(countryStates[0].states){	
	    		var stateList = '<select id="state_name" class="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" ref={register} name="state"><option value="">State</option>';
	    		for (const [i, state] of Object.entries(countryStates[0].states)) {
	    			stateList +=  '<option value="'+state.name+'">'+state.name+'</option>'
	    		}
	    		stateList +=  '</select>'

				$("#state_list").html(stateList)
			}else{
				$("#state_list").html('<input placeholder="State" id="state_name" class="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" ref={register} name="state" />');
			}
		}else{
			$("#state_list").html('<input placeholder="State" id="state_name" class="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" ref={register} name="state" />');
            $("#country").removeClass("floated");
		}
    }

    function predicateBy(prop){
       return function(a,b){
          if (a[prop] > b[prop]){
              return 1;
          } else if(a[prop] < b[prop]){
              return -1;
          }
          return 0;
       }
    }

    const countriesList = []
    countries.sort( predicateBy("name") )
    if(countries.length>0){
        for (const [i, country] of Object.entries(countries)) {
        	countriesList.push(
        		<option key={i} value={country.name}>{country.name}</option>
        	)
        }
    }

    function floatingLabel(e){
      var input = e.target
      $(input).parents(".floating_input").addClass("floated")
    }

    function floatingLabelOut(e){
      var input = e.target
      if(! $(input).val() ){
          $(input).parents(".floating_input").removeClass("floated")
      }
    }

	return (
		<Layout>
      <Head>
        <title>{registerData.seo ? registerData.seo.title_tag : 'Register'}</title>
        <meta name="title" content={registerData.seo ? registerData.seo.title_tag : ''}/>
        <meta name="description" content={registerData.seo ? registerData.seo.meta_description : ''}/>
        <link rel="canonical" href={siteUrl + '/register'} />
      </Head>
			<div className="login_register md:flex md:h-screen">
				<div style={{ backgroundImage: "url(" + registerData.image.url + ")" }} className="lr_left_image md-max:hidden md:bg-cover lg:w-1/2 md-tab:w-1/2 bg-login-img bg-no-repeat bg-center"></div>
				<div className="lr_right_section md:self-center md:justify-self-center md:flex-grow md:h-screen md:overflow-y-auto text-center">
					<div className="lg:w-4/6 lg:mx-auto p-8">
						<div className="logo mb-6 cursor-pointer">
                            <a href="/">
                                <img className="mx-auto" src={headerlogo.logo.url} alt={headerlogo.logo.url} width="112" height="53" />
                            </a>
                        </div>
						<h1 className="font-black my-8 text-6xl md-max:text-4xl text-gray-700">{registerData.title}</h1>
						<div className="my-8" dangerouslySetInnerHTML={{
                            __html: registerData.description
                            }} />
						<div className="tabs my-12">
							<Link href="/login"><a className="mx-8 text-lg uppercase">Login</a></Link>
							<Link href="/register"><a className="mx-8 uppercase font-bold underline text-lg">Register</a></Link>
						</div>
						<div className="login_tab">
							<div className="bg-green-200 py-3 px-4 border-2 border-green-300 border-dashed" id="success_message" style={{display: "none"}}>We've sent you an confiramation email. Please check your mail box to confirm your account!</div>
							<div className="error_message py-3 px-4 text-left bg-red-100 border-2 border-red-300 border-dashed" style={{display: "none"}}></div>
							<form className="form_login" onSubmit={handleSubmit(onSubmit)}>
								<div className="my-6 md:flex">
									<div className="md:w-1/2 md:pr-2 md-max:my-6 floating_input">
                                        <label>First Name</label>
										<input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text"  onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register} name="first_name" />
									</div>
									<div className="md:w-1/2 md:pl-2 floating_input">
                                        <label>Last Name</label>
										<input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register} name="last_name" />
									</div>
								</div>
								<div className="my-6 md:flex">
									<div className="md:w-1/2 md:pr-2 md-max:my-6 floating_input">
                                        <label>Display Name</label>
										<input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register} name="display_name" />
									</div>
									<div className="md:w-1/2 md:pl-2 floating_input">
                                        <label>Phone Number</label>
										<input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="number" onFocus={floatingLabel} onBlur={floatingLabelOut} name="phone_number" ref={register({ pattern: /\d+/ })} />
                                        {errors.phone_number && <div className="text-left text-red-500">Please enter valid number</div>}
									</div>
								</div>
								<div className="my-6 floating_input">
                                    <label>Email Address *</label>
									<input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="email" onFocus={floatingLabel} onBlur={floatingLabelOut}
                                        name="email" ref={register({
                                          required: "Please enter email address",
                                          pattern: {
                                            value: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                                            message: "Please enter a valid email address"
                                          }
                                        })}
                                    />
									{errors.email && <div className="text-left text-red-500">{errors.email.message}</div>}
								</div>
								<div className="my-6 md:flex">
									<div className="md:w-1/2 md:pr-2 md-max:my-6 floating_input">
                                        <label>Password *</label>
                                        <input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="password" name="password" autoComplete="new-password" ref={register({
                                                  required: "Please enter password",
                                                  minLength: {
                                                    value: 5,
                                                    message: "Password must have at least 5 characters"
                                                  }
                                                })}
                                            />
										{errors.password && <div className="text-left text-red-500">{errors.password.message}</div>}
									</div>
									<div className="md:w-1/2 md:pl-2 floating_input">
                                        <label>Confirm Password *</label>
										<input onFocus={floatingLabel} onBlur={floatingLabelOut} className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="password" name="confirm_password" ref={register({
                                                required: "Please enter confirm password",
                                                validate: value =>
                                                value === password.current || "Password & Confirm Password not match"
                                            })}
                                    />
                                    {errors.confirm_password && <div className="text-left text-red-500">{errors.confirm_password.message}</div>}
									</div>
								</div>
                                <div className="my-6 floating_input">
                                    <label>Birthday</label>
                                    <DatePicker
                                      name="birthday"
                                      ref={register}
                                      id="birthday"
                                      className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent"
                                      selected={startDate}
                                      onChange={date => setStartDate(date)}
                                      onFocus={floatingLabel} 
                                      onBlur={floatingLabelOut}
                                      peekNextMonth
                                      showMonthDropdown
                                      showYearDropdown
                                      dropdownMode="select"
                                      maxDate={new Date()}
                                      required
                                    />
                                    {/* {errors.birthday && <div className="text-left text-red-500">{errors.birthday.message}</div>} */}
                                </div>
								<div className="my-6 md:flex">
									<div className="md:w-1/2 md:pr-2 md-max:my-6 floating_input">
                                        <label>Street Address</label>
										<input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register} name="address_line_1" />
									</div>
									<div className="md:w-1/2 md:pl-2 floating_input">
                                        <label>Address Line 2</label>
										<input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register} name="address_line_2"/>
									</div>
								</div>
								<div className="my-6 md:flex">
									<div className="md:w-1/3 md:pr-4 md-max:my-6 floating_input" id="country">
                                        <label>Country</label>
										<select className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" onChange={e => handleStates(e.target.value) } ref={register} name="country">
											<option value=""></option>
											{countriesList}
										</select>
									</div>
									<div className="md:w-1/3 md:pr-2 md-max:my-6 floating_input">
                                        <label>Postal Code</label>
										<input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register} name="zipcode" />
									</div>
									<div className="md:w-1/3 md:pl-2 floating_input">
                                        <label>City</label>
										<input className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register} name="city" />
									</div>
								</div>
								<div className="my-6 floating_input" id="state_list">
                                    <label>State</label>
									<input id="state_name" className="w-full py-3 border-2 border-gray-300 focus:border-gray-700 focus:ring-transparent" type="text" onFocus={floatingLabel} onBlur={floatingLabelOut} ref={register} name="state" />
								</div>
								<div className="my-6">
									<button className="w-full uppercase px-2 py-4 font-bold text-white bg-green-700 hover:bg-black focus:outline-none" type="submit">register</button>
								</div>
							</form>
						</div>
						<div className="text-sm mt-24" dangerouslySetInnerHTML={{
                            __html: footerData.footer_warning_text
                            }} />
					</div>
				</div>
			</div>
        </Layout>
	);
}

export async function getServerSideProps() {

    const res_countries = await fetch(`https://api.printful.com/countries`)
    const countries  = await res_countries.json()

    const {API_URL} = process.env

    const res_registrationpage = await fetch(`${API_URL}/registration-page`)
    const registerData  = await res_registrationpage.json()

    const res_footer = await fetch(`${API_URL}/footer`)
    const footerData  = await res_footer.json()

    const res_headerlogo = await fetch(`${API_URL}/header-logo`)
    const headerlogo  = await res_headerlogo.json()

    const siteUrl = server

  return {
    props: {
      countries: countries.result,
      registerData,
      footerData,
      headerlogo,
      siteUrl
    }
  }
}

export default Register;