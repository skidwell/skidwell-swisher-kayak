import Head from "next/head";
import Layout from "../../components/layout";
import Getme from "../../components/getme";
import React, { useEffect, useState } from "react";
import Router , {useRouter}  from 'next/router';
import { useCookies } from "react-cookie";
import $ from "jquery";
import {categoryProducts} from "../../components/category_products"
import Link from "next/link";
import { server } from '../../config';
import getConfig from "next/config";

const Category = ({ products, slug, categories, hubs, footerNewsletter, categoryPageContent, currentCategory, siteUrl }) => {

  const router = useRouter();

  function makeBreadcrumbJobSchema() {
      return {
         "@context": "https://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "Home",
            "item": siteUrl
          },{
            "@type": "ListItem",
            "position": 2,
            "name": "Products",
            "item": siteUrl + "/products"
          },{
            "@type": "ListItem",
            "position": 3,
            "name": currentCategory.title
          }]
      }
  }
  
  return (

    <Layout>
        <Head>
          <title>{currentCategory.seo ? currentCategory.seo.title_tag : currentCategory.title }</title>
          <meta name="title" content={currentCategory.seo ? currentCategory.seo.title_tag : ''}/>
          <meta name="description" content={currentCategory.seo ? currentCategory.seo.meta_description : ''}/>
          <link rel="canonical" href={siteUrl + '/category/' + currentCategory.slug} />
          <script
            type='application/ld+json'
            dangerouslySetInnerHTML={{ __html: JSON.stringify(makeBreadcrumbJobSchema()) }}
          />
        </Head>
        <div className="product-section" id="product-section">
        <div className="container mx-auto px-4">
        <div className="products-head pt-10 pb-10 md:pb-24 md-max:-m-4 md-max:px-4 bg-green-700 text-center bg-main-pattern-t bg-bottom bg-x-full bg-no-repeat">
          <div className="text-lg text-white uppercase">{currentCategory.banner ? currentCategory.banner.tagline : ''}</div>
          <h1 id="page_title" className="text-3xl py-3 font-bold uppercase text-white">Category: {currentCategory.banner ? currentCategory.banner.title : currentCategory.title}</h1>
          <div className="mx-auto text-white" dangerouslySetInnerHTML={{
              __html: currentCategory.banner ? currentCategory.banner.description : ''
              }} />
        </div>      
        <div className="product_filters text-center py-10">
          <ul className="text-center">
            <li className="inline-block px-4 font-light">
              <Link href="/products">
                <a id="filter_all">View All Products</a>
              </Link>
            </li>
            {categories.map((category, i) => (
              <li key={i} className="border-l-2 border-solid border-gray-700 inline-block px-4 font-light">
                  <Link href={'/category/'+category.slug}>
                  <a className={slug === category.slug ? 'text-orange-700 font-bold mr-2 border-b-2 border-orange-700' : ''} id={'product_'+category.slug}>{category.title}</a></Link>
              </li>
            ))}
          </ul>
        </div>
        <div className="product-details max-w-5xl mx-auto">
            <div className="product_items">
              <div className="category_detsils pb-8">
              <h4 className="text-3xl font-bold uppercase pb-4 text-gray-700">{ currentCategory.title }</h4>
              <div className="text-gray-700 pb-6" dangerouslySetInnerHTML={{
                __html: currentCategory.description
                }} />
              </div>
              <div className="product-list flex flex-wrap md-max:flex-col pb-20 md:-mx-8">
                  {products.map((product, i) => (
                      <div  key={i} className="product-item md:text-center md:w-1/2 lg:w-1/3 p-8 md-max:px-0 md-max:py-2 md-max:flex">
                        <a href={`/product/${product.slug}`}>
                          <div className="product-image bg-gray-200 px-6 py-10 md-max:p-2 md-max:w-28">
                            <img className="mx-auto" src={product.image[0].url} alt={product.title} />
                          </div>
                        </a>
                        <div className="product-content p-6 border-3 border-solid border-gray-200 md-max:flex-grow">
                          <a href={`/product/${product.slug}`}>
                            <div className={ 'text-' + product.color + '-700 font-bold uppercase text-2xl md-max:text-lg product-title-category'}>{product.title}</div>
                            <div className="price font-semibold text-lg">${product.price}</div>
                          </a>
                        </div>
                      </div>
                  ))}
              </div>
            </div>
            <div className="bottom-content text-center pb-12">
              <div dangerouslySetInnerHTML={{
                __html: currentCategory.bottom_text
                }} />
            </div>
          </div>
        </div>
      <div className="blog_news px-4">
          <div className="mb-0 pt-20 pb-20 md:pb-96 md-tab:pb-56 md-tab:-mb-40 lg:-mb-72 text-center bg-gray-700 -mx-4 md-max:px-4 md:bg-main-pattern-t md:bg-x-full md:bg-bottom md:bg-no-repeat">
              <h3 className="text-white pb-4 uppercase text-lg">{categoryPageContent.tagline}</h3>
              <h2 className="text-3xl md-max:text-xl text-white font-bold uppercase pr-2 md-max:pb-4 md-max:text-center">{categoryPageContent.title}</h2>
          </div>
        <div className="container mx-auto">
          <div className="blog_news_items md-max:pb-12 md-max:bg-gray-700 md-max:-mx-4 md-max:px-4">
            <div className="container mx-auto">
              <div className="flex flex-wrap md-max:flex-col">
                {hubs.map((hub, i) => (
                <div key={i} className={i%2 == 0 ? 'blog_news_item md:w-1/2 md:pr-8 md-tab:pr-4 md-max:text-center pb-12' : 'blog_news_item md:w-1/2 md:pl-8 md-tab:pl-4 md-max:text-center pb-12'}>
                  <figure><img className="w-full" src={hub.list_image.url} alt={hub.list_image.url} /></figure>
                  <div className="">From {hub.hub_category.title}</div>
                  <div className="text-black font-bold uppercase text-2xl md-max:text-xl md-max:text-white">{hub.title}</div>
                  <Link href={`/hub-details/${hub.slug}`}>
                  <a className="underline text-orange-700 text-sm font-bold">Read Now</a>
                  </Link>
                </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
      <Getme footerNewsletter={footerNewsletter} />
      </div>
      </Layout>
  )
}

export async function getServerSideProps(context) {

  const slugArr = context.query.slug;
  const slug = slugArr[0]

  const {API_URL} = process.env
  var page = 1
  if (context.query.page){
    page = context.query.page
  }

  const start = +page === 1 ? 0 : (+page - 1) * 9

  const resproductCat = await fetch(`${API_URL}/categories?slug=${slug}`)
  const productCat = await resproductCat.json()

  const id = productCat[0].id

  const numberOfProductsResponse = await fetch(`${API_URL}/products/count?category=${id}`)
  const numberOfProducts = await numberOfProductsResponse.json()
  
  const res_products = await fetch(`${API_URL}/products?category=${id}`)
  const products  = await res_products.json()

  const res_categories = await fetch(`${API_URL}/categories`)
  const categories  = await res_categories.json()

  const res_hubs = await fetch(`${API_URL}/hubs?_limit=4&_sort=id:ASC`)
  const hubs  = await res_hubs.json()

  const res_footernewsletter = await fetch(`${API_URL}/footer-newsletter`)
  const footerNewsletter  = await res_footernewsletter.json()

  const res_categorypagecontent = await fetch(`${API_URL}/category-page-content`)
  const categoryPageContent  = await res_categorypagecontent.json()

  const siteUrl = server

  return {
    props: {
      products: products,
      slug: slug,
      categories: categories,
      hubs: hubs,
      footerNewsletter,
      categoryPageContent,
      currentCategory: productCat[0],
      siteUrl
    }
  }

}

export default Category
