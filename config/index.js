const dev = process.env.NODE_ENV !== 'production';

const BASE_URL = process.env.BASE_URL
const DEV_BASE_URL = process.env.BASE_URL

export const server = dev ? BASE_URL : DEV_BASE_URL;